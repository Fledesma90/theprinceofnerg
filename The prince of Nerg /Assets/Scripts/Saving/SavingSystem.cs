using System;
using System.Collections;
using System.Collections.Generic;
//usamos la libreria IO para guardar el archivo
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
//guardamos los objetos dentro de la escena
namespace theprinceofnerg.Saving
{
    public class SavingSystem : MonoBehaviour
    {
        /// <summary>
        /// Cargamos la última escena según nuestro último guardado
        /// </summary>
        /// <param name="saveFile"></param>
        /// <returns></returns>
        public IEnumerator LoadLastScene(string saveFile)
        {
            Dictionary<string, object> state = LoadFile(saveFile);
            int buildIndex = SceneManager.GetActiveScene().buildIndex;
            if (state.ContainsKey("lastSceneBuildIndex"))
            {
                buildIndex = (int)state["lastSceneBuildIndex"];
            }
            yield return SceneManager.LoadSceneAsync(buildIndex);
            RestoreState(state);
        }
        /// <summary>
        /// toma un string que es el objeto del que queremos guardar datos
        /// guardamos en ese objeto los estados en los que se encuentra la escena
        /// </summary>
        /// <param name="saveFile"></param>
        public void Save(string saveFile)
        {
            //le damos el valor de LoadFile
            Dictionary<string, object> state = LoadFile(saveFile);
            //capturamos el state
            CaptureState(state);
            //guardamos el estado en saveFile, es el resultado de todos los estados
            //que hemos realizado hasta ahora
            SaveFile(saveFile, state);
        }
        /// <summary>
        /// carga la escena con el estado según los datos guardados del archivo
        /// </summary>
        /// <param name="saveFile"></param>
        public void Load(string saveFile)
        {
            RestoreState(LoadFile(saveFile));
        }
        /// <summary>
        /// borra todos los datos guardados de este archivo
        /// </summary>
        /// <param name="saveFile"></param>
        public void Delete(string saveFile)
        {
            File.Delete(GetPathFromSaveFile(saveFile));
        }

        private Dictionary<string, object> LoadFile(string saveFile)
        {
            string path = GetPathFromSaveFile(saveFile);
            //verificamos si hay un archivo de guardado existente en el destino de guardado
            if (!File.Exists(path))
            {
                //sino creamos un nuevo diccionario
                return new Dictionary<string, object>();
            }
            //convertimos el archivo a guardar en binario
            using (FileStream stream = File.Open(path, FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return (Dictionary<string, object>)formatter.Deserialize(stream);
            }
        }
        /// <summary>
        /// guardamos el capturestate en state
        /// </summary>
        /// <param name="saveFile"></param>
        /// <param name="state"></param>
        private void SaveFile(string saveFile, object state)
        {
            string path = GetPathFromSaveFile(saveFile);
            print("Saving to " + path);
            //creamos un stream que es un archivo que podemos usar para leer datos
            using (FileStream stream = File.Open(path, FileMode.Create))
            {
                //creamos el formato binario del archivo para guardar datos serializables
                BinaryFormatter formatter = new BinaryFormatter();
                //pasamos el stream y el dato que pasaremos
                formatter.Serialize(stream, state);
            }
        }
        /// <summary>
        /// buscamos todas las saveableentities
        /// </summary>
        /// <param name="state"></param>
        private void CaptureState(Dictionary<string, object> state)
        {
            foreach (SaveableEntity saveable in FindObjectsOfType<SaveableEntity>())
            {
                state[saveable.GetUniqueIdentifier()] = saveable.CaptureState();
            }

            state["lastSceneBuildIndex"] = SceneManager.GetActiveScene().buildIndex;
        }
        /// <summary>
        /// Restauramos el estado del juego
        /// </summary>
        /// <param name="state"></param>
        private void RestoreState(Dictionary<string, object> state)
        {
            foreach (SaveableEntity saveable in FindObjectsOfType<SaveableEntity>())
            {
                string id = saveable.GetUniqueIdentifier();
                if (state.ContainsKey(id))
                {
                    saveable.RestoreState(state[id]);
                }
            }
        }
        /// <summary>
        /// Guardamos el archivo de datos con el nombre otorgado en el string
        /// dependiendo del SO el archivo se guardará en una localización u otra
        /// pero para facilitar el proceso hemos creado el script SHOWSAVEFILE que
        /// te muestra por un menú de unity creado gracias al script su ubicación
        /// </summary>
        /// <param name="saveFile"></param>
        /// <returns></returns>
        private string GetPathFromSaveFile(string saveFile)
        {
            return Path.Combine(Application.persistentDataPath, saveFile + ".sav");
        }
    }
}