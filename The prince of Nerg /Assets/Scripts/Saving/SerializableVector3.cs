//usamos la libreria unityengine para poder usar los vectores
using UnityEngine;

namespace theprinceofnerg.Saving
{
    //usamos serializable para que se pueda guardar la posición
    [System.Serializable]
    public class SerializableVector3
    {
        float x, y, z;

        public SerializableVector3(Vector3 vector)
        {
            x = vector.x;
            y = vector.y;
            z = vector.z;
        }
        /// <summary>
        /// reseteamos la posición del player
        /// </summary>
        /// <returns></returns>
        public Vector3 ToVector()
        {
            return new Vector3(x, y, z);
        }
    }
}