namespace theprinceofnerg.Saving
{
    //interfaaz de los objetos guardables
    public interface ISaveable
    {
        object CaptureState();
        void RestoreState(object state);
    }
}