using System;
//añadimos la libreria para usar los diccionarios
using System.Collections.Generic;
using theprinceofnerg.Core;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

namespace theprinceofnerg.Saving
{
    //pegamos este componente en todos los objetos en los que queramos guardar
    //con el que el sistema de guardado reconocerá y usará para captar su estado individual (STATE)
    // y lo pondrá en un diccionario que será serializable
    [ExecuteAlways]
    public class SaveableEntity : MonoBehaviour
    {
        //identificador del objeto según la escena en la que esté
        [SerializeField] string uniqueIdentifier = "";
        //nos aseguramos de la singularidad del identificable entre escenas
        static Dictionary<string, SaveableEntity> globalLookup = new Dictionary<string, SaveableEntity>();

        public string GetUniqueIdentifier()
        {
            return uniqueIdentifier;
        }
        /// <summary>
        /// guardamos el estado del objeto
        /// </summary>
        /// <returns></returns>
        public object CaptureState()
        {
            Dictionary<string, object> state = new Dictionary<string, object>();
            foreach (ISaveable saveable in GetComponents<ISaveable>())
            {
                state[saveable.GetType().ToString()] = saveable.CaptureState();
            }
            return state;
        }
        /// <summary>
        /// restauramos el estado del objeto
        /// </summary>
        /// <param name="state"></param>
        public void RestoreState(object state)
        {
            Dictionary<string, object> stateDict = (Dictionary<string, object>)state;
            foreach (ISaveable saveable in GetComponents<ISaveable>())
            {
                string typeString = saveable.GetType().ToString();
                if (stateDict.ContainsKey(typeString))
                {
                    saveable.RestoreState(stateDict[typeString]);
                }
            }
        }
        //el contenido de unity editor no va a estar incluido en la build de c#
#if UNITY_EDITOR
        private void Update() {
            //verificamos si el objeto que tiene este script esta en playmode
            if (Application.IsPlaying(gameObject)) return;
            if (string.IsNullOrEmpty(gameObject.scene.path)) return;
            //encuentra el contenido serializable de este objeto
            SerializedObject serializedObject = new SerializedObject(this);
            //buscamos un campo particular de este objeto serializable
            SerializedProperty property = serializedObject.FindProperty("uniqueIdentifier");
            
            if (string.IsNullOrEmpty(property.stringValue) || !IsUnique(property.stringValue))
            {
                property.stringValue = System.Guid.NewGuid().ToString();
                serializedObject.ApplyModifiedProperties();
            }
            //actualizamos la identidad del objeto con sus datos guardados
            globalLookup[property.stringValue] = this;
        }
#endif
        /// <summary>
        /// verificamos si este objeto existe en el diccionario
        /// </summary>
        /// <param name="candidate"></param>
        /// <returns></returns>
        private bool IsUnique(string candidate)
        {
            if (!globalLookup.ContainsKey(candidate)) return true;

            if (globalLookup[candidate] == this) return true;

            if (globalLookup[candidate] == null)
            {
                globalLookup.Remove(candidate);
                return true;
            }

            if (globalLookup[candidate].GetUniqueIdentifier() != candidate)
            {
                globalLookup.Remove(candidate);
                return true;
            }

            return false;
        }
    }
}