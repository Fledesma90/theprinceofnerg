﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.SceneManagement;
using theprinceofnerg.Saving;
public class IntroCinematics : MonoBehaviour, ISaveable {
    #region ############## VARIABLES
    //objetos activos de la escena cuando no está en intro
    public GameObject normalWorld;
    //objetos activos de la intro
    public GameObject introWorld;
    //booleana para definir si está en intro o no
    public bool isIntro;
    //cambiaremos de camara cuando este acabandose la intro
    public GameObject camToActivate;
    [Header("Dialogues")]
    //numero de diálogos
    [SerializeField] int dialogueNumber = 4;
    //los textos son los diálogos a mostrar en la inro
    public GameObject text1;
    public GameObject text2;
    public GameObject text3;
    public GameObject text4;
    //objeto que usaremos para cambiar de escena
    GameObject portalIntro;
    #endregion

    #region ############## EVENTS

    void Start()
    {
        //referencia al portal
        portalIntro = GameObject.Find("PortalIntro2");
        //si es true cambiamos de musica y activamos los objetos de la intro
        if (isIntro == true) {
            MusicPlayer musicPlayer;
            musicPlayer = FindObjectOfType<MusicPlayer>();
            musicPlayer.ChangeMusic();
            introWorld.SetActive(true);
            normalWorld.SetActive(false);
        }
        if (isIntro==false) {
            
            portalIntro.GetComponent<BoxCollider>().enabled = false;
        }
    }


    void Update() {
        //cada veez que hagamos click cambiarmoes de diálogo
        DialogueDisplay();
        if (Input.GetMouseButtonDown(0) && isIntro == true) {
            dialogueNumber -= 1;
        }
    }

    #endregion

    #region ############# METHODS
    /// <summary>
    /// mostramos los diálogos además de cambiar de escena
    /// </summary>
    void DialogueDisplay() {
        if (dialogueNumber==4) {
            text1.SetActive(true);
        } else {
            text1.SetActive(false);
        }
        if (dialogueNumber==3) {
            text2.SetActive(true);
        } else {
            text2.SetActive(false);
        }
        if (dialogueNumber==2) {
            text3.SetActive(true);
        } else {
            text3.SetActive(false);
        }
        if (dialogueNumber==1) {
            text4.SetActive(true);
        } else {
            text4.SetActive(false);
        }
        if (dialogueNumber==0) {
            
            portalIntro.GetComponent<BoxCollider>().enabled = true;
            isIntro = false;
            normalWorld.SetActive(true);
            introWorld.SetActive(false);
            camToActivate.SetActive(true);
        }
    }
    /// <summary>
    /// guardamos el estado
    /// </summary>
    /// <returns></returns>
    public object CaptureState() {
        return isIntro;
    }
    public void RestoreState(object state) {
        isIntro = (bool)state;
    }
    #endregion
}
