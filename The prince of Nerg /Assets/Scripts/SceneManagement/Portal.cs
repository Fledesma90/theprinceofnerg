﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.AI;
namespace theprinceofnerg.SceneManagement {
    public class Portal : MonoBehaviour {
        enum DestinationIdentifier {
            A, B, C, D, E, F
        }
        //nombre de la escena
        public string sceneToLoad;
        public AudioClip openSound;
        public AudioClip closeSound;
        public AudioSource audioSource;
        [SerializeField] Transform spawnPoint;
        [SerializeField] DestinationIdentifier destination;
        //tiempo que dura la animación de fadeout
        [SerializeField] float fadeOutTime = 1f;
        //tiempo que dura el fade in
        [SerializeField] float fadeInTime = 2f;
        //tiempo de espera entre fade in y fade out
        [SerializeField] float fadeWaitTime = 0.5f;
        bool playerIn;
        private void OnTriggerEnter(Collider other) {
            if (other.tag == "Player") {
                playerIn = true;
            }
            if (other.tag == "Player" || other.tag == "FakePlayer") {
                StartCoroutine(Transition());
                
            }

        }

        /// <summary>
        /// averiguamos cuando cambiamos de escena
        /// </summary>
        /// <returns></returns>
        public IEnumerator Transition() {
            //no destruimos el portal
            DontDestroyOnLoad(gameObject);
            if (gameObject.tag != "Portal2") {
                audioSource.PlayOneShot(closeSound);
            }
            //llamamos y buscamos al fader entre escenas
            Fader fader = FindObjectOfType<Fader>();
            yield return fader.FadeOut(fadeOutTime);

            //guardamos la escena actual
            SavingWrapper wrapper = FindObjectOfType<SavingWrapper>();
            wrapper.Save();
            yield return SceneManager.LoadSceneAsync(sceneToLoad);
            //cargamos la escena
            wrapper.Load();
            if (playerIn == true) {
                //portal del siguiente nivel
                Portal otherPortal = GetOtherPortal();
                UpdatePlayer(otherPortal);
                wrapper.Save();
            }
            //una vez que se ha desplazado el personaje esperamos un tiempo para el fadein
            yield return new WaitForSeconds(fadeWaitTime);
            //realizamos el fadein porque ya hemos pasado a la siguiente escena
            yield return fader.FadeIn(fadeInTime);
            if (gameObject.tag != "Portal2") {
                audioSource.PlayOneShot(openSound);
            }
            //destruimos este portal
            Destroy(gameObject);

        }
        /// <summary>
        /// Instanciamos el player en la posición de spawn del portal
        /// </summary>
        /// <param name="otherPortal"></param>
        private void UpdatePlayer(Portal otherPortal) {
            GameObject player = GameObject.FindWithTag("Player");
            player.GetComponent<NavMeshAgent>().Warp(otherPortal.spawnPoint.position);
            player.transform.rotation = otherPortal.spawnPoint.rotation;
        }
        /// <summary>
        /// buscamos los portales 
        /// </summary>
        /// <returns></returns>
        private Portal GetOtherPortal() {
            foreach (Portal portal in FindObjectsOfType<Portal>()) {
                if (portal == this) continue;
                if (portal.destination != destination) continue;
                return portal;
            }
            return null;
        }
    }
}