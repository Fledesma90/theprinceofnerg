﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//usamos el namespace saving para quepodamos usar los sistemas de guardados
using theprinceofnerg.Saving;
namespace theprinceofnerg.SceneManagement {
    public class SavingWrapper : MonoBehaviour {
        //Este será el archivo que guardaremos por defecto con el nombre que le otorgaremos
        const string defaultSaveFile = "save";
        [SerializeField] float fadeInTime = 0.2f;
        /// <summary>
        /// inmediatamente cargamos la última escena abierta
        /// </summary>
        /// <returns></returns>
        IEnumerator Start() {
            Fader fader = FindObjectOfType<Fader>();
            fader.FadeOutImmediate();
            yield return GetComponent<SavingSystem>().LoadLastScene(defaultSaveFile);
            yield return fader.FadeIn(fadeInTime);
        }
        private void Update() {
            if (Input.GetKeyDown(KeyCode.L)) {
                Load();
            }
            if (Input.GetKeyDown(KeyCode.S)) {
                Save();
            }
        }
        /// <summary>
        /// Método para cargar la escena de manera pública para que sea accesible
        /// desde los demás scripts
        /// </summary>
        public void Load() {
            GetComponent<SavingSystem>().Load(defaultSaveFile);
        }

        public void Save() {
            GetComponent<SavingSystem>().Save(defaultSaveFile);
        }

        public void Delete() {
            GetComponent<SavingSystem>().Delete(defaultSaveFile);
        }
    }
}