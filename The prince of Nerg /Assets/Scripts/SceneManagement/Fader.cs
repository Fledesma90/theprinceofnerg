﻿using UnityEngine;
using System.Collections;
namespace theprinceofnerg.SceneManagement{
    public class Fader : MonoBehaviour {

        CanvasGroup canvasGroup;

        private void Start() {
            canvasGroup = GetComponent<CanvasGroup>();
        }

        public void FadeOutImmediate() {
            canvasGroup.alpha = 1;
        }

        public IEnumerator FadeOut(float time) {
            //realizamos esta accion durante una cantidad de tiempo limitada
            while (canvasGroup.alpha<1) {
                canvasGroup.alpha += Time.deltaTime / time;
                yield return null;
            }
        }
        public IEnumerator FadeIn(float time) {
            //realizamos esta accion durante una cantidad de tiempo limitada
            while (canvasGroup.alpha > 0) {
                canvasGroup.alpha -= Time.deltaTime / time;
                yield return null;
            }
        }
    }
}


