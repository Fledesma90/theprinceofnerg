﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//hacemos referencia a la libreria UI para poder usar texto en el codigo
using UnityEngine.UI;
public class DialogueWriter : MonoBehaviour
{
    #region ############## VARIABLES
    //velocidad en la que aparece cada caracter
    public float delay = 0.1f;
    [TextArea]
    //lo que se va a mostrar cuando el texto haya terminado de escribirse
    public string fullText;
    //nivel de texto escrito actual
    private string currentText = "";
    #endregion

    #region ############## EVENTS

    void Start()
    {
        //llamamos a la corrutina al activar el objeto en la escena
        StartCoroutine(ShowText());
    }


    void Update()
    {
        
    }
    #endregion

    #region ############# METHODS
    /// <summary>
    /// Método en el que usaremos la corrutina y el efecto máquina de escribir
    /// </summary>
    /// <returns></returns>
    IEnumerator ShowText() {
        for (int i = 0; i < fullText.Length + 1; i++) {
            currentText = fullText.Substring(0, i);
            this.GetComponent<Text>().text = currentText;
            yield return new WaitForSeconds(delay);
        }
    }
    #endregion
}
