﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.Control;
using theprinceofnerg.Saving;
public class ExitCastle : MonoBehaviour, ISaveable {
    #region ############## VARIABLES
    //referncia al jugador
    GameObject player;
    //referencia al portal del poblado
    GameObject portalPoblado;
    //referencia al diálogo 
    GameObject canvasNoKey;
    GameObject canvasKey;
    //bool para definir si esta activo o no el collider del objeto
    [SerializeField]bool isCollider;
    //collider
    BoxCollider boxCollider;
    AudioSource audioSource;
    public AudioClip noExitSound;
    public AudioClip exitSound;
  #endregion

  #region ############## EVENTS

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        player = GameObject.Find("Player");
        boxCollider = GetComponent<BoxCollider>();
        portalPoblado = GameObject.Find("PortalPoblado");
        canvasNoKey = GameObject.Find("CanvasNoKey");
        canvasKey = GameObject.Find("CanvasKey");
        canvasNoKey.SetActive(false);
        canvasKey.SetActive(false);
        //activamos el collider de este objeto o lo desactivamos al igual que el portal
        // del poblado dependiendo del estado de la bool
        if (isCollider==false) {
            portalPoblado.SetActive(true);
            boxCollider.enabled = false;
        }
        if (isCollider==true) {
            boxCollider.enabled = true;
            portalPoblado.SetActive(false);
        }
    }


    void Update()
    {
        
    }
    #endregion

    #region ############# METHODS
    /// <summary>
    /// si entra el player en el collider y no tiene la llave realizamos la corrutina NoExit
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other) {
        if (other.tag=="Player"&& player.GetComponent<InventoryManager>().isKey == false) {
            StartCoroutine(NoExit());
        }
        /// <summary>
        /// si entra el player en el collider y tiene la llave realizamos la corrutina Exit
        /// </summary>
        /// <param name="other"></param>
        if (other.tag == "Player" && player.GetComponent<InventoryManager>().isKey == true) {
            StartCoroutine(Exit());
            Invoke("OpenThis", 6);
        }
    }
    /// <summary>
    /// El personaje no se podrá mover durante 4 segundos para que el jugador
    /// pueda leer el texto 
    /// </summary>
    /// <returns></returns>
    IEnumerator NoExit() {
        DisableMovements();
        audioSource.PlayOneShot(noExitSound);
        canvasNoKey.SetActive(true);
        yield return new WaitForSeconds(4);
        canvasNoKey.SetActive(false);
        EnabledMovements();
    }

    /// <summary>
    /// El personaje no se podrá mover durante 4 segundos para que el jugador
    /// pueda leer el texto. Ademas desaparecerá la llave, el collider del objeto también y no podrá
    /// ver más la llave el jugador
    /// </summary>
    /// <returns></returns>
    IEnumerator Exit() {
        player.GetComponent<PlayerController>().canNotMove = true;
        canvasKey.SetActive(true);
        audioSource.PlayOneShot(exitSound);
        player.GetComponent<Animator>().SetBool("canInteract", true);
        yield return new WaitForSeconds(4);
        player.GetComponent<Animator>().SetBool("canInteract", false);
        player.GetComponent<PlayerController>().canNotMove = false;
        player.GetComponent<InventoryManager>().isKey = false;
        player.GetComponent<InventoryManager>().hudKey.SetActive(false);
        canvasKey.SetActive(false);
        //nos aseguramos que este en falso
        isCollider = false;
        
        boxCollider.enabled = false;
    }
    /// <summary>
    /// activamos el portal
    /// </summary>
    void OpenThis() {
        portalPoblado.SetActive(true);
    }
    /// <summary>
    /// deshabilitamos movimientos
    /// </summary>
    void DisableMovements() {
        player.GetComponent<PlayerController>().canNotMove = true;
        player.GetComponent<Animator>().SetBool("Talk", true);
    }
    /// <summary>
    /// habilitamos movimientos
    /// </summary>
    void EnabledMovements() {
        player.GetComponent<PlayerController>().canNotMove = false;
        player.GetComponent<Animator>().SetBool("Talk", false);
    }
    /// <summary>
    /// estado a guardar
    /// </summary>
    /// <returns></returns>
    public object CaptureState() {
        return isCollider;

    }

    public void RestoreState(object state) {

        isCollider = (bool)state;
    }
    #endregion
}
