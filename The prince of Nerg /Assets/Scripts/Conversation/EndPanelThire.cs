﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.SceneManagement;
public class EndPanelThire : MonoBehaviour
{
    #region ############## VARIABLES
    //referencia al savingwrapper para borrar los datos
    SavingWrapper savingWrapper;
    #endregion

    #region ############## EVENTS

    void Start()
    {
        //recogemos referencias
        savingWrapper = GameObject.Find("Saving").GetComponent<SavingWrapper>();
    }


    void Update()
    {
        
    }
    #endregion

    #region ############# METHODS
    /// <summary>
    /// Cerramos el juego y borramos datos
    /// </summary>
    public void QuitGame() {

        savingWrapper.Delete();
        Application.Quit();
    }

    #endregion
}
