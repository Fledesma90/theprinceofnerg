﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.Control;
namespace theprinceofnerg.Conversation {
    public class MercurioDialogue : MonoBehaviour {
        #region ############## VARIABLES
        //canvas del dialogo por defecto
        public GameObject canvasDefault1;
        //animator
        Animator anim;
        //player
        [SerializeField] GameObject player;

        #endregion

        #region ############## EVENTS

        void Start() {
            //recuperamos las referencias
            player = GameObject.FindGameObjectWithTag("Player");
            anim = GetComponent<Animator>();
        }


        void Update() {

        }
        #endregion

        #region ############# METHODS
        public void MercurioConversation() {
            StartCoroutine(MercurioCorru());
        }
        /// <summary>
        /// realizamos corrutina para que aparezca el diálogo durante unos segundos
        /// y el personaje no podrá andar hasta pasado dicho intervalo de tiempo
        /// </summary>
        /// <returns></returns>
        IEnumerator MercurioCorru() {
            anim.SetBool("Talk", true);
            DisableMovements();
            canvasDefault1.SetActive(true);
            yield return new WaitForSeconds(4);
            canvasDefault1.SetActive(false);
            EnabledMovements();
            anim.SetBool("Talk", false);
        }
        void DisableMovements() {
            player.GetComponent<PlayerController>().canNotMove = true;
        }

        void EnabledMovements() {
            player.GetComponent<PlayerController>().canNotMove = false;
        }
        #endregion
    }

}
