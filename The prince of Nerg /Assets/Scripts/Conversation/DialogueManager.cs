﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using theprinceofnerg.Control;
using theprinceofnerg.Combat;
using theprinceofnerg.Saving;
using theprinceofnerg.Movement;
using theprinceofnerg.SceneManagement;
using theprinceofnerg.UI;
namespace theprinceofnerg.Conversation {
    public class DialogueManager : MonoBehaviour,ISaveable {
        #region ############## VARIABLES
        //componente audiosource
        AudioSource audioSource;
        //audioclip para comenzar la prueba
        public AudioClip firstTrialSound;
        //audioclip que sonará como recompensa 
        public AudioClip successSound;
        //booleanas para determinar los diálogos que corresponden
        public bool isThire1;
        public bool isThire2;
        public bool isThire3;
        //lista de los diálogos
        public List<GameObject> dialogues = new List<GameObject>();
        //elemento de texto que estaran dentro de la lista
        public Text textDialogue;
        //objeto padre de los diálogos
        public GameObject dialogueParent;
        //numero de diálogo
        private int dialoguesNumber = 0;
        //particle system
        public GameObject ps;
        //copia del mago Thire 
        public GameObject thireCopy;
        //posicionamiento en el mundo de las copias del mago
        public Transform[] thireCopyTransforms;
        //referencia a la puerta
        public GameObject portal;
        //contador para activar y desactivar componentes al terminar el diálogo
        public float timer = 5;
        //bool para determinar cuando realizamos el contador
        public bool timerOn=false;
        public bool timerOn2 = false;
        //si encontramos al mago original las copias mueren
        public bool dieCopies;
        //jugador
        public GameObject player;
        [Header("Outro")]
        //objeto bestia que sera el player
        public GameObject beast;
        //modelo del player
        public GameObject playerModel;
        //textos de la outro
        public GameObject text1;
        public GameObject text2;
        public GameObject text3;
        public GameObject text4;
        public GameObject text5;
        public GameObject text6;
        public GameObject fadeTransition;
        public GameObject outro;
        public GameObject text7;
        public GameObject text8;
        public GameObject text9;
        public GameObject text10;
        public GameObject text11;
        public GameObject text12;
        public GameObject text13;
        //panel de fin del juego
        public GameObject endPanel;
        //texto para informarnos que debemos hacer click sobre el mago para pasar los diálogos
        public GameObject text;
        #endregion

        #region ############## EVENTS


        void Start() {
            //recuperamos la referencia al audioSource
            audioSource = GetComponent<AudioSource>();
            //buscamos la referencia del menu de fin de juego
            endPanel = GameObject.Find("EndMenuUI");
            //desactivamos el panel hasta que acabemos el juego
            endPanel.SetActive(false);
            //buscamos el modelo del player
            playerModel = GameObject.Find("Kale");
            //buscamos el objeto player
            player = GameObject.FindGameObjectWithTag("Player");
            //buscamos la bestia
            beast = GameObject.Find("Bestia");
            //desactivamos la bestia ya que aparecerá cuando se beba la poción el jugador
            beast.SetActive(false);
            //si el diálogo segundo es verdadero entonces la condicion de que se reproduzca
            //el primero es falsa y rellenaremos el objeto padre de diálogos con los elementos de texto
            //que encontramos en el objeto de diálogo 2
            if (isThire2==true) {
                isThire1 = false;
                dialogueParent = GameObject.FindGameObjectWithTag("dialogue2");
                foreach (Transform item in dialogueParent.transform) {
                    dialogues.Add(item.gameObject);
                }
            }
            //si la booleana isThire1 es verdadera rellenamos el objeto padre con los elementos del objeto
            //contenedor de textos que no tiene etiqueta
            if (isThire1) {
                //rellenamos la lista
                foreach (Transform item in dialogueParent.transform) {
                    dialogues.Add(item.gameObject);
                }
            }
          //si uno de los dos es verdadero podremos salir de la sala, esto es para evitar que el player
          //pueda salir de la escena sin haber matado a todas las copias del mago
            if (isThire1||isThire2) {
                portal = GameObject.FindGameObjectWithTag("Portal");
            }
            //chequeamos la condición de que tenga la poción y llevamos a cabo el diálogo final
            if (player.GetComponent<InventoryManager>().isPotion==true) {
                isThire1 = false;
                isThire2 = false;
                isThire3 = true;
                timer = 0;
            }
        }

        void Update() {
            //manejamos los contadores con booleanas y realizamos las tareas necesarias
            if (timerOn) {
                timer -= Time.deltaTime;
                if (timer <= 0) {
                    //ponemos el valor de las booleanas según los diálogos que deban
                    //aparecer acorde con el momento de la historia
                    isThire1 = false;
                    isThire2 = true;
                    gameObject.layer = 0;
                    dialoguesNumber = 0;
                    dialogueParent = GameObject.FindGameObjectWithTag("dialogue2");
                    //rellenamos la lista
                    foreach (Transform item in dialogueParent.transform) {
                        dialogues.Add(item.gameObject);
                    }
                    timerOn = false;
                }
            }
            if (timerOn2) {
                timer += Time.deltaTime;
                Thire3Actions();

                if (timer>=34) {
                    beast.SetActive(false);
                }
                if (timer >= 33 && timer <= 34) {

                    outro.SetActive(true);
                }
                if (timer>65) {
                    timerOn2 = false;
                }
                
            }
        }
        #endregion

        #region ############# METHODS
        /// <summary>
        /// Método del que llamaremos con el raycast
        /// </summary>
        public void Dialogue() {
            if (isThire1) {
                DialogueThire1();
            }
            if (isThire2) {
                player.GetComponent<InventoryManager>().canSeeMoney = true;
                isThire1 = false;
                DialogueThire2();
            }

            if (isThire3) {
                isThire1 = false;
                isThire2 = false;
                DialogueThire3();
            }
           
        }
        /// <summary>
        /// Efectuamos el diálogo primero, inhabilitando el movimiento del jugador y estableciendo la prueba
        /// de instanciar copias del mago tiniendo que averiguar el personaje cual es la copia original
        /// </summary>
        void DialogueThire1() {
            DisableMovements();
            if (dialoguesNumber > 0) {
                dialogues[dialoguesNumber - 1].SetActive(false);
            }

            if (dialoguesNumber < dialogues.Count) {
                dialogues[dialoguesNumber].SetActive(true);
            }
            dialoguesNumber++;

            if (dialoguesNumber > dialogues.Count) {
                EnabledMovements();
                portal.GetComponent<BoxCollider>().enabled = false;
                audioSource.PlayOneShot(firstTrialSound);
                foreach (var thireCopyTransform in thireCopyTransforms) {
                    Instantiate(ps, thireCopyTransform.position + new Vector3(0, 2, 0), thireCopyTransform.rotation);
                    Instantiate(thireCopy, thireCopyTransform.position, thireCopyTransform.rotation);
                    text.SetActive(false);
                }
                
                Instantiate(ps, transform.position, transform.rotation);
                dialogues.Clear();
                timerOn = true;
                gameObject.layer = 2;
               
            }
        }
        /// <summary>
        /// Efectuamos el diálogo 2 informando al jugador que se ha superado la prueba
        /// se cambiará la lista del objeto padre de los diálogos por la lista de los diálogos
        /// pertenecientes al diálogo 2
        /// </summary>
        void DialogueThire2() {
            
            DisableMovements();
            dieCopies = true;
            if (dialoguesNumber==0) {
                audioSource.PlayOneShot(successSound);
            }
            if (dialoguesNumber > 0) {
                portal.GetComponent<BoxCollider>().enabled = true;
                FindObjectsOfType<CombatTarget>();
                dialogues[dialoguesNumber - 1].SetActive(false);
                
            }
            if (dialoguesNumber<dialogues.Count) {
                dialogues[dialoguesNumber].SetActive(true);
                dialoguesNumber++;
            }
            if (dialoguesNumber>=dialogues.Count) {
                EnabledMovements();
            }
        }
        /// <summary>
        /// Diálogo final del juego en el que se activará el contador  cuya función
        /// será activar y desactivar diálogos según el tiempo que haya transcurrido
        /// hasta acabar el juégo
        /// </summary>
        void DialogueThire3() {
            timerOn2 = true;
            player.GetComponent<PlayerController>().enabled = false;
            player.GetComponent<ShowHideUI>().enabled = false;
            
        }
        /// <summary>
        /// Acciones que cumplirá el código según el valor del tiempo
        /// </summary>
        private void Thire3Actions() {
            player.GetComponent<Animator>().SetBool("Talk", true);
            if (timer <= 5) {
                text1.SetActive(true);
            } else {
                text1.SetActive(false);
            }
            if (timer >= 5&&timer<=10) {
                text2.SetActive(true);
            } else {
                text2.SetActive(false);
            }
            if (timer >= 10&&timer<=15) {
                text3.SetActive(true);
            } else {
                text3.SetActive(false);
            }
            if (timer >= 15 && timer <= 20) {
                player.GetComponent<Animator>().SetBool("Talk", false);
                player.GetComponent<Animator>().SetBool("Yes", true);
               
                text4.SetActive(true);
            } else {
                player.GetComponent<Animator>().SetBool("Yes", false);
                text4.SetActive(false);
            }

            if (timer >= 20 && timer <= 25) {
                playerModel.SetActive(false);
                beast.SetActive(true);
                beast.GetComponent<Animator>().SetBool("idle", true);
                player.GetComponent<InventoryManager>().hudPotion.SetActive(false);
                text5.SetActive(true);
            } else {
                text5.SetActive(false);
                beast.GetComponent<Animator>().SetBool("idle", false);
            }
            if (timer >= 25 && timer <= 29) {
                    beast.transform.Translate(-0.01f, 0, 0);
                    beast.GetComponent<Animator>().SetBool("Walk", true);
                    text6.SetActive(true);
            } else {
                beast.GetComponent<Animator>().SetBool("Walk", false);
                text6.SetActive(false);
            }
            
            if (timer >= 29&&timer<=34) {
                fadeTransition.SetActive(true);
                
            } else {
                
                fadeTransition.SetActive(false);
            }
            
            if (timer >= 35 && timer <= 40) {
                text7.SetActive(true);
                
            } else {
                text7.SetActive(false);
            }
            
            if (timer >= 40 && timer <= 45) {
                text8.SetActive(true);

            } else {
                text8.SetActive(false);
            }
            if (timer >= 45 && timer <= 50) {
                text9.SetActive(true);
            } else {
                text9.SetActive(false);
            }
            if (timer >= 50 && timer <= 55) {
                text10.SetActive(true);
            } else {
                text10.SetActive(false);
            }
            if (timer >= 55 && timer <= 60) {
                text11.SetActive(true);
            } else {
                text11.SetActive(false);
            }
            if (timer >= 60 && timer <= 65) {
                text12.SetActive(true);
            } else {
                text12.SetActive(false);
            }
            if (timer >= 65) {
                endPanel.SetActive(true);
            }
        }
        /// <summary>
        /// Deshabilitamos los movimientos del jugador
        /// </summary>
        void DisableMovements() {
            player.GetComponent<PlayerController>().canNotMove = true;
            if (gameObject.GetComponent<LookAtPlayer>().isTalkAnim) {
                player.GetComponent<Animator>().SetBool("Talk", true);
            }
        }
        /// <summary>
        /// Habilitamos los movimientos del player
        /// </summary>
        void EnabledMovements() {
            player.GetComponent<PlayerController>().canNotMove = false;
            player.GetComponent<Animator>().SetBool("Talk", false);

        }
        /// <summary>
        /// estado a guardar para que funcione el juego
        /// </summary>
        /// <returns></returns>
        public object CaptureState() {
            return isThire2;
            
        }

        public void RestoreState(object state) {
            
            isThire2 = (bool)state;
        }
        #endregion
    }
}