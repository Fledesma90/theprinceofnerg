﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.Control;
using theprinceofnerg.Saving;
using theprinceofnerg.Movement;
namespace theprinceofnerg.Conversation {
    public class HiedraDialogue : MonoBehaviour, ISaveable {
        #region ############## VARIABLES
        //booleanas para determinar qué diálogo mostrar
        public bool isHiedra1;
        public bool isHiedra2;
        public bool isDefault1;
        public bool isDefault2;
        public bool isDefault3;
        //objeto padre de la lista de textos a mostrar
        public GameObject dialogueParent;
        //canvas de los diálogos por defecto
        public GameObject canvasDefault1;
        public GameObject canvasDefault2;
        public GameObject canvasDefault3;
        //bool para activar temporizador
        public bool timerOn = false;
        //temporizador que dependerá de la booleana 
        public float timer = 2f;
        //lista de diálogos
        public List<GameObject> dialogues = new List<GameObject>();
        //numero de la lista de diálogos 
        private int dialoguesNumber = 0;
        //animator
        Animator anim;
        //player
        [SerializeField] GameObject player;
        //AudioSource
        AudioSource audioSource;
        //sonido de comprar
        public AudioClip cashSound;
        #endregion

        #region ############## EVENTS

        void Start() {
            //referencia al audiosource
            audioSource = GetComponent<AudioSource>();
            //referencia al player
            player = GameObject.FindGameObjectWithTag("Player");
            //referencia al animator
            anim = GetComponent<Animator>();
            Checks();
            if (isHiedra1==true) {
                dialogueParent = GameObject.FindGameObjectWithTag("dialogue1");
                //rellenamos la lista
                foreach (Transform item in dialogueParent.transform) {
                    dialogues.Add(item.gameObject);
                }
            }
            if (isHiedra2 == true) {
                dialogueParent = GameObject.FindGameObjectWithTag("dialogue2");
                foreach (Transform item in dialogueParent.transform) {
                    dialogues.Add(item.gameObject);
                }
            }
        }

        

        void Update() {
            if (timerOn==true) {
                timer -= Time.deltaTime;
                if ((timer<=0&&isHiedra1==true) || (timer <= 0 && isHiedra2 == true)) {
                    isDefault3 = true;
                    isHiedra1 = false;
                    isHiedra2 = false;
                    timerOn = false;
                    timer = 2;
                    dialogueParent.SetActive(false);
                }
            }
        }
        #endregion

        #region ############# METHODS
        public void HiedraConversation() {
            if (isDefault1==true) {
                HiedraDefaultDialogue1();
            }
            if (isHiedra1==true) {
                HiedraConversation1();
            }
            if (isHiedra2==true) {
                HiedraConversation2();
            }
            if (isDefault2==true) {
                HiedraDefaultDialogue2();
            }
            if (isDefault3 == true) {
                HiedraDefaultDialogue3();
            }
        }

        void HiedraConversation1() {
            DisableMovements();
            anim.SetBool("Talk", true);
            if (dialoguesNumber > 0) {
                dialogues[dialoguesNumber - 1].SetActive(false);
            }
            if (dialoguesNumber < dialogues.Count) {
                dialogues[dialoguesNumber].SetActive(true);
                dialoguesNumber++;
            }
            
            if (dialoguesNumber >= dialogues.Count) {
                player.GetComponent<InventoryManager>().hudWine.SetActive(true);
                player.GetComponent<InventoryManager>().hudMoney.SetActive(false);
                player.GetComponent<InventoryManager>().isWine = true;
                player.GetComponent<InventoryManager>().isMoney = false;
                player.GetComponent<InventoryManager>().canBuyWine = false;
                player.GetComponent<InventoryManager>().canSeeMoney = true;
                EnabledMovements();
                anim.SetBool("Talk", false);
                timerOn = true;
            }
        }

        void HiedraConversation2() {
            DisableMovements();
            anim.SetBool("Talk", true);
            if (dialoguesNumber > 0) {
                dialogues[dialoguesNumber - 1].SetActive(false);
            }
            if (dialoguesNumber < dialogues.Count) {
                dialogues[dialoguesNumber].SetActive(true);
                dialoguesNumber++;
            }
            if (dialoguesNumber >= dialogues.Count) {
                player.GetComponent<InventoryManager>().hudDew.SetActive(true);
                player.GetComponent<InventoryManager>().hudMoney.SetActive(false);
                player.GetComponent<InventoryManager>().isDew = true;
                player.GetComponent<InventoryManager>().isMoney = false;
                player.GetComponent<InventoryManager>().canBuyDew = false;
                EnabledMovements();
                anim.SetBool("Talk", false);
                timerOn = true;

            }
        }

        void HiedraDefaultDialogue1() {
            StartCoroutine(HiedraCorru1());
        }

        void HiedraDefaultDialogue2() {
            StartCoroutine(HiedraCorru2());
        }

        void HiedraDefaultDialogue3() {
            StartCoroutine(HiedraCorru3());
        }

       IEnumerator HiedraCorru1() {
            anim.SetBool("Yes", true);
            DisableMovements();
            canvasDefault1.SetActive(true);
            yield return new WaitForSeconds(4);
            canvasDefault1.SetActive(false);
            EnabledMovements();
            anim.SetBool("Yes", false);
        }
        IEnumerator HiedraCorru2() {
            anim.SetBool("Yes", true);
            DisableMovements();
            canvasDefault2.SetActive(true);
            yield return new WaitForSeconds(4);
            canvasDefault2.SetActive(false);
            EnabledMovements();
            anim.SetBool("Yes", false);
        }
        IEnumerator HiedraCorru3() {
            anim.SetBool("Yes", true);
            DisableMovements();
            canvasDefault3.SetActive(true);
            yield return new WaitForSeconds(4);
            canvasDefault3.SetActive(false);
            EnabledMovements();
            anim.SetBool("Yes", false);
        }
        void DisableMovements() {
            player.GetComponent<PlayerController>().canNotMove = true;
            if (gameObject.GetComponent<LookAtPlayer>().isTalkAnim) {
                player.GetComponent<Animator>().SetBool("Talk", true);
            }
        }

        void EnabledMovements() {
            player.GetComponent<PlayerController>().canNotMove = false;
            player.GetComponent<Animator>().SetBool("Talk", false);
        }
        /// <summary>
        /// verificaciones a realizar del inventario del player, en caso de que tenga dinero
        /// podrá comprar lo que le ofrezca la buhonera en cada momento
        /// </summary>
        private void Checks() {
            if (player.GetComponent<InventoryManager>().isWine == true ||
                            player.GetComponent<InventoryManager>().isDew == true ||
                            player.GetComponent<InventoryManager>().isPotion == true) {
                isDefault3 = true;
            }
            if (player.GetComponent<InventoryManager>().canBuyWine == true &&
                player.GetComponent<InventoryManager>().isMoney == false) {
                isDefault1 = true;
            }

            if (player.GetComponent<InventoryManager>().canBuyDew == true &&
                player.GetComponent<InventoryManager>().isMoney == false) {
                isDefault3 = false;
                isDefault2 = true;
            }

            if (player.GetComponent<InventoryManager>().canBuyWine == true &&
                player.GetComponent<InventoryManager>().isMoney == true) {
                isHiedra1 = true;
                isDefault1 = false;
            }

            if (player.GetComponent<InventoryManager>().canBuyDew == true &&
                player.GetComponent<InventoryManager>().isMoney == true) {
                isDefault3 = false;
                isDefault2 = false;
                isHiedra2 = true;
            }
        }
        /// <summary>
        /// variables a guardar en el struct
        /// </summary>
        [System.Serializable]
        struct HiedraSaveData {
            public bool isHiedra1;
            public bool isHiedra2;
            public bool isDefault1;
            public bool isDefault2;
            public bool isDefault3;
        }
        /// <summary>
        /// igualamos las variables del struct 
        /// </summary>
        /// <returns></returns>
        public object CaptureState() {

            HiedraSaveData data = new HiedraSaveData();
            data.isHiedra1 = isHiedra1;
            data.isHiedra2 = isHiedra2;
            data.isDefault1 = isDefault1;
            data.isDefault2 = isDefault2;
            data.isDefault3 = isDefault3;
            return data;
        }
        /// <summary>
        /// guardamos datos entre escenas
        /// </summary>
        /// <param name="state"></param>
        public void RestoreState(object state) {

            HiedraSaveData data = (HiedraSaveData)state;
            isHiedra1 = data.isHiedra1;
            isHiedra2 = data.isHiedra2;
            isDefault1 = data.isDefault1;
            isDefault2 = data.isDefault2;
            isDefault3 = data.isDefault3;
        }
        #endregion
    }

}
