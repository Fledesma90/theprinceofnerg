﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.Control;
using UnityEngine.UI;
using theprinceofnerg.Movement;
using theprinceofnerg.Saving;
namespace theprinceofnerg.Conversation {
    public class RikaDialogue : MonoBehaviour, ISaveable {
        #region ############## VARIABLES
        public bool isRika1;
        public bool isRika2;
        public bool isRika3;
        public bool isDefault;
        public bool timerOn = false;
        public float timer = 2f;
        public List<GameObject> dialogues = new List<GameObject>();
        public GameObject dialogueParent;
        public GameObject defaultDialogue;
        private int dialoguesNumber = 0;
        Animator anim;
        [SerializeField] GameObject player;
        #endregion

        #region ############## EVENTS

        void Start() {
            player = GameObject.FindGameObjectWithTag("Player");
            anim = GetComponent<Animator>();
            if (isRika1==true) {
                dialogueParent = GameObject.FindGameObjectWithTag("dialogue1");
                //rellenamos la lista
                foreach (Transform item in dialogueParent.transform) {
                    dialogues.Add(item.gameObject);
                }
            }
            if (player.GetComponent<InventoryManager>().isWine == true) {
                isRika2 = true;
                isRika1 = false;
                isDefault = false;

            }
            if (isRika2 == true) {
                dialogueParent = GameObject.FindGameObjectWithTag("dialogue2");
                foreach (Transform item in dialogueParent.transform) {
                    dialogues.Add(item.gameObject);
                }
            }
            if (player.GetComponent<InventoryManager>().isDew == true) {
                isRika2 = false;
                isRika3 = true;
                isDefault = false;

            }
            if (isRika3 == true) {
                isRika1 = false;
                isRika2 = false;
                isDefault = false;
                dialogueParent = GameObject.FindGameObjectWithTag("dialogue3");
                foreach (Transform item in dialogueParent.transform) {
                    dialogues.Add(item.gameObject);
                }
            }

        }
        private void FixedUpdate() {
            
        }
        void Update() {
            if (timerOn==true) {
                
                timer -= Time.deltaTime;
                if ((timer<=0&&isRika1==true)|| (timer <= 0 && isRika2 == true)|| (timer <= 0 && isRika3 == true)) {
                    isDefault = true;
                    isRika1 = false;
                    isRika2 = false;
                    isRika3 = false;
                    timerOn = false;
                    timer = 2;
                    dialogueParent.SetActive(false);
                }
                
            }

        }
        #endregion

        #region ############# METHODS

        public void RikaConversation() {
            if (isRika1==true) {
                RikaConversation1();
            }

            if (isRika2==true) {
                RikaConversation2();
            }

            if (isRika3) {
                RikaConversation3();
            }

            if (isDefault) {
                RikaDefault();
            }
        }

        void RikaConversation1() {
            DisableMovements();
            anim.SetBool("Talk", true);
                if (dialoguesNumber > 0) {
                    dialogues[dialoguesNumber - 1].SetActive(false);
                }
                if (dialoguesNumber < dialogues.Count) {
                    dialogues[dialoguesNumber].SetActive(true);
                    dialoguesNumber++;
                }
                if (dialoguesNumber >= dialogues.Count) {
                player.GetComponent<InventoryManager>().canBuyWine = true;
                player.GetComponent<InventoryManager>().canSeeKey = true;
                EnabledMovements();
                anim.SetBool("Talk", false);
                timerOn = true;
               
                }
            
        }

        void RikaConversation2() {
            anim.SetBool("Talk", true);
            DisableMovements();

            if (dialoguesNumber > 0) {
                dialogues[dialoguesNumber - 1].SetActive(false);
            }
            if (dialoguesNumber < dialogues.Count) {
                dialogues[dialoguesNumber].SetActive(true);
                dialoguesNumber++;
            }
            if (dialoguesNumber >= dialogues.Count) {
                player.GetComponent<InventoryManager>().isWine = false;
                player.GetComponent<InventoryManager>().hudWine.SetActive(false);
                player.GetComponent<InventoryManager>().canBuyDew = true;
                EnabledMovements();
                anim.SetBool("Talk", false);
                timerOn = true;

            }
        }

        void RikaConversation3() {
            anim.SetBool("Talk", true);
            DisableMovements();

            if (dialoguesNumber > 0) {
                dialogues[dialoguesNumber - 1].SetActive(false);
            }
            if (dialoguesNumber < dialogues.Count) {
                dialogues[dialoguesNumber].SetActive(true);
                dialoguesNumber++;
            }
            if (dialoguesNumber >= dialogues.Count) {
                player.GetComponent<InventoryManager>().isDew = false;
                player.GetComponent<InventoryManager>().hudDew.SetActive(false);
                player.GetComponent<InventoryManager>().canBuyDew = true;
                player.GetComponent<InventoryManager>().hudPotion.SetActive(true);
                player.GetComponent<InventoryManager>().isPotion = true;
                EnabledMovements();
                anim.SetBool("Talk", false);
                timerOn = true;
            }
        }

        void RikaDefault() {
            StartCoroutine(RikaDefaultCorru());
        }

         IEnumerator RikaDefaultCorru() {
            anim.SetBool("Yes", true);
            DisableMovements();
            defaultDialogue.SetActive(true);
            yield return new WaitForSeconds(4);
            defaultDialogue.SetActive(false);
            EnabledMovements();
            anim.SetBool("Yes", false);
        }

        void DisableMovements() {
            player.GetComponent<PlayerController>().canNotMove = true;
            if (gameObject.GetComponent<LookAtPlayer>().isTalkAnim) {
                player.GetComponent<Animator>().SetBool("Talk", true);
            }
        }

        void EnabledMovements() {
            player.GetComponent<PlayerController>().canNotMove = false;
            player.GetComponent<Animator>().SetBool("Talk", false);
        }

        [System.Serializable]
        struct RikaSaveData {
            public bool isRika1;
            public bool isRika2;
            public bool isRika3;
            public bool isDefault;
        }

        public object CaptureState() {

            RikaSaveData data = new RikaSaveData();
            data.isRika1 = isRika1;
            data.isRika2 = isRika2;
            data.isRika3 = isRika3;
            data.isDefault = isDefault;
            return data;
        }

        public void RestoreState(object state) {

            RikaSaveData data = (RikaSaveData)state;
            isRika1 = data.isRika1;
            isRika2 = data.isRika2;
            isRika3 = data.isRika3;
            isDefault = data.isDefault;
        }
        #endregion
    }

}
