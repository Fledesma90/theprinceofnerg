﻿using UnityEngine;
using theprinceofnerg.Movement;
using theprinceofnerg.Core;
using theprinceofnerg.Conversation;
namespace theprinceofnerg.Conversation {
    public class PlayerConversation : MonoBehaviour, IAction {
        #region ############## VARIABLES
        //rango para hablar
        [SerializeField] float talkRange = 2f;
        //hacia donde nos dirigimos
        Transform target;
        //referencia al script mover
        public Mover mover;
        [SerializeField] Animator anim;
        public bool canTalk;
        #endregion

        #region ############## EVENTS

        void Start() {

        }


        void Update() {
            //si el target es null no hacemos ninguno de estos comportamientos
            if (target == null) return;
            if (!GetIsInRange()) {
                mover.MoveTo(target.position);
                canTalk = false;
            } else {
                //si estamos en el alcance paramos
                mover.Cancel();
                transform.LookAt(target.position);
                TalkBehaviour();
            }
        }
        #endregion

        #region ############# METHODS
       
        private bool GetIsInRange() {
            return Vector3.Distance(transform.position, target.position) < talkRange;
        }
         private void TalkBehaviour() {
            canTalk = true;
            transform.LookAt(target.transform.position);
            //anim.SetTrigger("Talk");
        }
        public void ThireConversation(DialogueManager dialogueManager) {
            GetComponent<ActionScheduler>().StartAction(this);
            target = dialogueManager.transform;
            //anim.SetTrigger("Talk");
            ThireDialogueFunction();
        }

        public void ThireDialogueFunction() {
            target.GetComponent<DialogueManager>().Dialogue();
        }

        public void RikaConversation(RikaDialogue rikaDialogue) {
            GetComponent<ActionScheduler>().StartAction(this);
            target = rikaDialogue.transform;
            RikaDialogueFunction();
        }

        public void RikaDialogueFunction() {
            target.GetComponent<RikaDialogue>().RikaConversation();
        }

        public void HiedraConversation(HiedraDialogue hiedraDialogue) {
            GetComponent<ActionScheduler>().StartAction(this);
            target = hiedraDialogue.transform;
            HiedraDialogueFunction();
        }

        public void HiedraDialogueFunction() {
            target.GetComponent<HiedraDialogue>().HiedraConversation();
        }

        public void KingQueenConver(MercurioDialogue mercurioDialogue) {
            GetComponent<ActionScheduler>().StartAction(this);
            target = mercurioDialogue.transform;
            KingQueenFunction();
        }

        public void KingQueenFunction() {
            target.GetComponent<MercurioDialogue>().MercurioConversation();
        }
        /// <summary>
        /// cancelamos el objetivo para que podamos movernos a otro lugar
        /// que no sea el objetivo
        /// </summary>
        public void Cancel() {
            //anim.SetTrigger("stopAction");
            target = null;
        }

        #endregion
    }
}


