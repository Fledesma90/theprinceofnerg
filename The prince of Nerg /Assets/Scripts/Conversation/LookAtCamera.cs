﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
  #region ############## VARIABLES
  #endregion

  #region ############## EVENTS

    void Start()
    {
        
    }


    void Update()
    {
        //hacemos que el objeto mire a la camar principal
        gameObject.transform.LookAt(Camera.main.transform);
    }
  #endregion

  #region ############# METHODS

  #endregion
}
