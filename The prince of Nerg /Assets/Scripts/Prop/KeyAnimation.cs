﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyAnimation : MonoBehaviour
{
    #region ############## VARIABLES
    //velocidad del efecto
    public float duration = 3.0F;
    //referencia a la luz
    public Light lt;
    public float degreesPerSecond = 15.0f;
    public float amplitude = 0.5f;
    public float frequency = 1f;
    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();
    #endregion

    #region ############## EVENTS

    void Start()
    {
        //recuperamos el componente light
        lt = GetComponent<Light>();
        posOffset = transform.position;
    }

    
    void Update() {
        BlinkingLightFX();
        FloatAndRotateFX();
    }

    #endregion

    #region ############# METHODS
    /// <summary>
    /// Efecto de parpadeo de luces
    /// </summary>
    private void BlinkingLightFX() {
        float phi = Time.time / duration * 2 * Mathf.PI;
        float amplitude = Mathf.Cos(phi) * 0.5F + 0.5F;
        lt.intensity = amplitude;
    }

    private void FloatAndRotateFX() {
        //Rota el objeto en el ejeY
        transform.Rotate(new Vector3(0f, Time.deltaTime * degreesPerSecond, 0f), Space.World);

        // Flota arriba y abajo 
        tempPos = posOffset;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        transform.position = tempPos;
    }
    #endregion
}
