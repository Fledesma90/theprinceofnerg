﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : MonoBehaviour
{
    #region ############## VARIABLES
    //player
    GameObject player;
    //modelo 3D
    GameObject model;
    //Objeto
    GameObject money;
    //panel de aviso bolsa llena
    public GameObject panel;
    //referencia audiosource
    AudioSource audioSource;
    //sonido de monedas
    public AudioClip coinSound;
    #endregion

    #region ############## EVENTS

    void Start()
    {
        //buscamos las referencias 
        audioSource = GetComponent<AudioSource>();
        money = GameObject.Find("Money");
        player = GameObject.Find("Player");
        model = GameObject.Find("Monedas");
        //si el jugador puede ver el dinero el modelo será visible 
        if (player.GetComponent<InventoryManager>().canSeeMoney==true) {
            GetComponent<BoxCollider>().enabled = true;
            model.SetActive(true);
            GetComponent<Light>().enabled = true;
            GetComponent<KeyAnimation>().enabled = true;
        } else {
            GetComponent<BoxCollider>().enabled = false;
            model.SetActive(false);
            GetComponent<Light>().enabled = false;
            GetComponent<KeyAnimation>().enabled = false;
        }
    }


    void Update()
    {
        
    }
    #endregion

    #region ############# METHODS
    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player"&& player.GetComponent<InventoryManager>().canSeeMoney ==true) {
            player.GetComponent<InventoryManager>().canSeeMoney = false;
            player.GetComponent<InventoryManager>().isMoney = true;
            player.GetComponent<InventoryManager>().hudMoney.SetActive(true);
            money.GetComponent<BoxCollider>().enabled = false;
            model.SetActive(false);
            audioSource.PlayOneShot(coinSound);
            Destroy(gameObject,1f);
        }
        if (other.tag == "Player" && player.GetComponent<InventoryManager>().canSeeMoney == false) {
            StartCoroutine(panelCorru());
        }
    }

    IEnumerator panelCorru() {
        panel.SetActive(true);
        yield return new WaitForSeconds(2);
        panel.SetActive(false);
    }
    #endregion
}
