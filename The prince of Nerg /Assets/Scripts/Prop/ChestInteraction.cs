﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.Control;
using theprinceofnerg.Saving;
using theprinceofnerg.Prop;
using theprinceofnerg.Combat;
    public class ChestInteraction : MonoBehaviour, ISaveable {
        #region ############## VARIABLES
        public Animator anim;
        [SerializeField] bool isOpen=false;
        float openCounter = 1f;
    [SerializeField] GameObject portal;
    [SerializeField] GameObject kaleText;
    BoxCollider boxCollider;
    //referencia al audiosource
    private AudioSource audioSource;
    public AudioClip openSound;
    public AudioClip swordEquippedSound;
    public GameObject ps;
    #endregion

    #region ############## EVENTS

    void Start() {
        boxCollider = GetComponent<BoxCollider>();
        audioSource = GetComponent<AudioSource>();
        //si hemos abierto el cofre lo dejamos cerrado
        if (isOpen==true) {
            boxCollider.enabled = false;
            portal.GetComponent<BoxCollider>().enabled = true;
            kaleText.SetActive(false);
        } else {
            boxCollider.enabled = true;
            portal.GetComponent<BoxCollider>().enabled = false;
            kaleText.SetActive(true);
        }
        }


        void Update() {

        }
    #endregion

    #region ############# METHODS

    private void OnTriggerEnter(Collider other) {
        if (other.tag=="Player") {
            portal.GetComponent<BoxCollider>().enabled = true;
            StartCoroutine(OpeningChest());
        }
    }

    IEnumerator OpeningChest() {
        Debug.Log("can");
        GameObject player;
        player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<PlayerController>().canNotMove = true;
        player.GetComponent<Animator>().SetBool("canInteract", true);
        anim.SetBool("isOpen", true);
        audioSource.PlayOneShot(openSound);
        Invoke("ParticleSystemPlay", 0.2f);
        yield return new WaitForSeconds(3f);
        audioSource.PlayOneShot(swordEquippedSound);

        player.GetComponent<PlayerController>().canNotMove = false;
        player.GetComponent<Animator>().SetBool("canInteract", false);
        anim.SetBool("isOpen", false);
        boxCollider.enabled = false;
        portal.SetActive(true);
        kaleText.SetActive(false);
        player.GetComponent<Fighter>().isSword = true;
        player.GetComponent<Fighter>().sword.SetActive(true);
        isOpen = true;
    }

    void ParticleSystemPlay() {
        ps.SetActive(true);
    }
    /// <summary>
    /// guardamos el estado
    /// </summary>
    /// <returns></returns>
    public object CaptureState() {
        return isOpen;
    }
    public void RestoreState(object state) {
        isOpen = (bool)state;
    }
    #endregion
}



