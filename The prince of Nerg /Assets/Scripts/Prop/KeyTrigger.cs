﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyTrigger : MonoBehaviour
{
    #region ############## VARIABLES
    GameObject player;
    GameObject model;
    public AudioClip soundKey;
    AudioSource audioSource;
    public GameObject ps;
  #endregion

  #region ############## EVENTS

    void Start()
    {
        //si el player puede ver la llave aparecerá de lo contrario
        //este objeto permaecerá invisible
        player = GameObject.Find("Player");
        model = GameObject.Find("Model");
        audioSource = GetComponent<AudioSource>();
        if (player.GetComponent<InventoryManager>().canSeeKey == true) {
            GetComponent<BoxCollider>().enabled = true;
            model.SetActive(true);
            GetComponent<Light>().enabled = true;
            GetComponent<KeyAnimation>().enabled = true;
        } else {
            GetComponent<BoxCollider>().enabled = false;
            model.SetActive(false);
            GetComponent<Light>().enabled = false;
            GetComponent<KeyAnimation>().enabled = false;
        }
    }


    void Update()
    {
        
    }
    #endregion

    #region ############# METHODS
    private void OnTriggerEnter(Collider other) {
        if (other.tag=="Player") {
            player.GetComponent<InventoryManager>().isKey = true;
            player.GetComponent<InventoryManager>().hudKey.SetActive(true);
            player.GetComponent<InventoryManager>().canSeeKey = false;
            audioSource.PlayOneShot(soundKey);
            Instantiate(ps, transform.position, transform.rotation);
            GetComponent<BoxCollider>().enabled = false;
            model.SetActive(false);
            Destroy(gameObject,2);
        }
    }
    #endregion
}
