﻿
using UnityEngine;
using theprinceofnerg.Conversation;
namespace theprinceofnerg.Combat {
    public class EnemyHealth : MonoBehaviour {
        #region ############## VARIABLES
        //puntos de vida
        public float healthPoints = 10f;
        //animator para las animaciones a realizar
        Animator anim;
        //bool para comprobar si han de morirse
        bool isDead=false;
        //sistema de partículas
        public GameObject ps;
        //referencia al dialoguemanager
        public DialogueManager dialogueManager;
        #endregion

        #region ############## EVENTS

        void Start() {
            //recuperamos la referencia al animator
            anim = GetComponent<Animator>();
            //recuperamos la referencia al dialogue manager
            dialogueManager = GameObject.FindWithTag("Thire").GetComponent<DialogueManager>();
        }
        /// <summary>
        /// llamamos al método de morir en caso de que hayamos encontrado
        /// la copia original del mago
        /// </summary>
        void Update() {
            if (dialogueManager.dieCopies == true) {
                gameObject.GetComponent<CapsuleCollider>().enabled = false;
                Die();
            }
        }
        #endregion

        #region ############# METHODS
        /// <summary>
        /// podemos quitar daño pero nunca menos que 0
        /// </summary>
        /// <param name="damage"></param>
        public void TakeDamage(float damage) {
            healthPoints = Mathf.Max(healthPoints - damage, 0);
            if (healthPoints > 0) {
                anim.SetTrigger("Hit");
            }
            if (healthPoints <= 0 || dialogueManager.dieCopies==true){
                gameObject.GetComponent<CapsuleCollider>().enabled = false;
                Die();
            }
        }
        /// <summary>
        /// Realizamos las acciones necesarias para el efecto de la muerte
        /// </summary>
        public void Die() {
            if (isDead) return;
            isDead = true;
            anim.SetTrigger("Die");
            Invoke("DieParticles", 0.9f);
            Destroy(gameObject, 1f);
        }
        /// <summary>
        /// efecto de las partículas
        /// </summary>
        private void DieParticles() {
            Instantiate(ps, transform.position, transform.rotation);

        }
        #endregion
    }

}
