﻿
using UnityEngine;
using theprinceofnerg.Movement;
using theprinceofnerg.Core;
using theprinceofnerg.Saving;

namespace theprinceofnerg.Combat {
    public class Fighter : MonoBehaviour,IAction, ISaveable {
        #region ############## VARIABLES

        //rango de ataque
        [SerializeField] float weaponRange = 2f;
        //hacia donde nos dirigimos
        Transform target;
        //referncia al script mover
        public Mover mover;
        [SerializeField] Animator anim;
        //tiempo entre ataques
        [SerializeField] float timeBetweenAttacks=1f;
        private float timeSinceLastAttack=0f;
        [SerializeField] float damage;
        //objeeto a instanciar cuando tengamos el arma
        public GameObject sword;
        public bool isSword;
        #endregion

        #region ############## EVENTS

        void Start() {
            WieldSword();
            anim = GetComponent<Animator>();
        }

        void Update() {
            //realizamos un contador para poder atacar
            timeSinceLastAttack += Time.deltaTime;
            //si el target es null no hacemos ninguno de estos comportamientos
            if (target == null) return;
            //si estamos lejos del campo de ataque vamos hacia el target
            if (!GetIsInRange()) {
                    mover.MoveTo(target.position);

            } else {
                //si estamos en el alcance paramos
                mover.Cancel();
                    AttackBehaviour();
            }
        }

        private void AttackBehaviour() {
            transform.LookAt(target.transform.position);
            //establecemos un contador para pulir el ritmo del ataque
            if (timeSinceLastAttack>timeBetweenAttacks && target.GetComponent<EnemyHealth>().healthPoints>0) {
                anim.SetTrigger("Attack");
                
                timeSinceLastAttack = 0;

            }
        }
        /// <summary>
        /// Realizamos las acciones pertenentes del ActionEvent de la animación de atacar
        /// </summary>
        void Hit() {
            //si no hay objetivo no ejecutamos el event
            if (target == null) { return; }
            EnemyHealth healthComponent = target.GetComponent<EnemyHealth>();
            healthComponent.TakeDamage(damage);
        }
        /// <summary>
        ///verificamos si estamos en el alcance de ataque del objetivo
        /// </summary>
        /// <returns></returns>
        private bool GetIsInRange() {
            return Vector3.Distance(transform.position, target.position) < weaponRange;
        }
        /// <summary>
        /// verificamos que el player tenga el arma con una booleana
        /// y activamos el objeto
        /// </summary>
        void WieldSword() {
            if (isSword==true) {
                sword.SetActive(true);
            } else {
                sword.SetActive(false);
            }
        }

        //private void OnTriggerEnter(Collider other) {
        //    if (other.GetComponent<ChestInteraction>()) {
        //        isSword = true;
        //        sword.SetActive(true);
        //    }
        //}
        #endregion

        #region #############
        /// <summary>
        /// 
        /// </summary>
        /// <param name="combatTarget"></param>
        public void Attack(CombatTarget combatTarget) {
            //cancelamos la action anterior y establecemos esta
            GetComponent<ActionScheduler>().StartAction(this);
            //establecemos la posición para atacar
            target = combatTarget.transform;
           
        }
        /// <summary>
        /// cancelamos el objetivo para que podamos movernos a otro lugar
        /// que no sea el objetivo
        /// </summary>
        public void Cancel() {
            target = null;
        }

        public object CaptureState() {
            return isSword;

        }

        public void RestoreState(object state) {

            isSword = (bool)state;
        }

        #endregion
    }

}