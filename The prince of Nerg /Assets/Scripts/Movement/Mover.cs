﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using theprinceofnerg.Core;
using theprinceofnerg.Saving;
namespace theprinceofnerg.Movement {
    //añadimos el action además del monobehaviour para los cambios de namespace
    //cuando sean necesarios
    public class Mover : MonoBehaviour, IAction, ISaveable {
        #region ############## VARIABLES
        //hacia donde se dirige el personaje
        [SerializeField] Transform target;
        [SerializeField] Animator animator;
        public NavMeshAgent nav;

        public Camera cam;
        #endregion

        #region ############## EVENTS

        void Start() {
            nav = GetComponent<NavMeshAgent>();
            cam = FindObjectOfType<Camera>();
            animator = GetComponent<Animator>();
        }


        void Update() {
            UpdateAnimator();
        }
        #endregion

        #region ############# METHODS

        /// <summary>
        /// actualizamos la velocidad con la animación
        /// </summary>
        private void UpdateAnimator() {
            Vector3 velocity = nav.velocity;
            //no importa hacia donde estés moviendote, siempre lo tomará como forward
            Vector3 localVelocity = transform.InverseTransformDirection(velocity);
            //queremos la velocidad en z porque es hacia donde corre la animación
            float speed = localVelocity.z;
            animator.SetFloat("forwardSpeed", speed);
        }
        /// <summary>
        /// este metodo lo cancelamos cuando hacemos click
        /// </summary>
        /// <param name="destination"></param>
        public void StartMoveAction(Vector3 destination) {
            //cancelamos la action anterior y establecemos esta
            GetComponent<ActionScheduler>().StartAction(this);
            MoveTo(destination);
        }

        /// <summary>
        /// dirección hacia la que va a ir el navmeshagent, lo hacemos cada fotograma
        /// </summary>
        /// <param name="destination"></param>
        public void MoveTo(Vector3 destination) {
            
            nav.destination = destination;
            //reanudamos el movimiento 
            nav.isStopped = false;
        }
        /// <summary>
        /// paramos el navmeshagent y cancelamos la action de fighter
        /// </summary>
        public void Cancel() {
            //paramos el navmeshagent con isStopped
            nav.isStopped = true;
        }
        /// <summary>
        /// guardamos nuestra posición
        /// </summary>
        /// <returns></returns>
        public object CaptureState() {
            return new SerializableVector3(transform.position);
        }
        /// <summary>
        /// Reseteamos la posición del playerrequiere un objeto por el
        /// principio de sustitución, ya sea mover, fighter...dependiendo de la acción.
        /// </summary>
        /// <param name="state"></param>
        public void RestoreState(object state) {
            SerializableVector3 position = (SerializableVector3)state;
            GetComponent<NavMeshAgent>().enabled = false;
            transform.position = position.ToVector();
            GetComponent<NavMeshAgent>().enabled = true;
        }
        #endregion
    }
}
    