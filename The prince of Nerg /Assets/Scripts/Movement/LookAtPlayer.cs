﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.Conversation;
namespace theprinceofnerg.Movement {
    public class LookAtPlayer : MonoBehaviour {
        #region ############## VARIABLES
        //player
        [SerializeField] Transform target;
        [SerializeField] float turnSpeed;
        //distancia mínima para mirar al jugador
        public float minDistance;
        public bool isTalkAnim;
        public GameObject text;
        #endregion

        #region ############## EVENTS

        void Start() {
            target = FindObjectOfType<Mover>().transform;
        }


        void Update() {
            FaceTarget();
        }
        #endregion

        #region ############# METHODS
        private void OnDrawGizmos() {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, minDistance);
        }
        /// <summary>
        /// Miramos al player
        /// </summary>
        private void FaceTarget() {
            if (Vector3.Distance(transform.position, target.position)<minDistance) {
                text.SetActive(true);
                isTalkAnim = true;
                Vector3 direction = (target.position - transform.position).normalized;
                Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
                //ponemos un Slerp para que rote de manera suave
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed);
            }
            if (Vector3.Distance(transform.position, target.position) > minDistance) {
                isTalkAnim = false;              
                text.SetActive(false);
            }
        }

        #endregion
    }
}

