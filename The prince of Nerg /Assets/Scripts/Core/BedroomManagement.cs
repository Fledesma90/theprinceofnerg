﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using theprinceofnerg.Saving;
using theprinceofnerg.SceneManagement;
public class BedroomManagement : MonoBehaviour, ISaveable
{
    #region ############## VARIABLES
    //text del narrador
    public GameObject narratorText;
    //referencia cofre
    public GameObject chest;
    //referencia llave
    public GameObject key;
    //canvas group
    CanvasGroup cv;
    //si este es verdadero veremos el texto del narrador
    [SerializeField] bool isNarratorText;
    float fadeOut=3f;
    float fadeIn = 3f;
  #endregion

  #region ############## EVENTS

    void Start()
    {
        cv = narratorText.GetComponent<CanvasGroup>();
    }


    void Update()
    {
        //si es true realizamos la corrutina para que aparezca temporalmente
        //el texto y desaparezca
        if (isNarratorText == true) {
            StartCoroutine(NarratorText());
        }
    }
    #endregion

    #region ############# METHODS

    IEnumerator NarratorText() {
        narratorText.SetActive(true);
        cv.alpha += Time.deltaTime / fadeIn;
        yield return new WaitForSeconds(fadeIn);
        cv.alpha -= Time.deltaTime / fadeOut;
        yield return new WaitForSeconds(fadeOut);
        isNarratorText = false;
    }

    /// <summary>
    /// guardamos el estado
    /// </summary>
    /// <returns></returns>
    public object CaptureState() {
        return isNarratorText;
    }
    public void RestoreState(object state) {
       isNarratorText = (bool)state;
    }
    #endregion
}
