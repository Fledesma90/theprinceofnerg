﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.Combat;
public class IntroPortal : MonoBehaviour
{
    #region ############## VARIABLES
    public GameObject player;
  #endregion

  #region ############## EVENTS

    void Start()
    {
        //cuando el player tenga la espada se desactiva el collider
        player = GameObject.Find("player");
        if (player.GetComponent<Fighter>().isSword==true) {
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
        if (player.GetComponent<Fighter>().isSword == false) {
            gameObject.GetComponent<BoxCollider>().enabled = true;
        }
    }


    void Update()
    {
        
    }
  #endregion

  #region ############# METHODS

  #endregion
}
