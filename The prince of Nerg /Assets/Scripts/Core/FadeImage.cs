﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class FadeImage : MonoBehaviour
{
    #region ############## VARIABLES
    CanvasGroup canvasGroup;
    //tiempos de fade in y fade ouy 
    [SerializeField] float timeFadeIn = 0.5f;
    [SerializeField] float timeFadeOut = 1;
    [SerializeField] float timeWaitFade=2f;
    #endregion

    #region ############## EVENTS

    void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        StartCoroutine(TransitionImage());
    }


    void Update()
    {
        
    }
    #endregion

    #region ############# METHODS
    public IEnumerator TransitionImage() {
        yield return FadeIn(timeFadeIn);
        yield return new WaitForSeconds(timeWaitFade);
        yield return FadeOutImage(timeFadeOut);
    }

    public IEnumerator FadeOutImage(float time) {
        //realizamos esta accion durante una cantidad de tiempo limitada
        while (canvasGroup.alpha < 1) {
            canvasGroup.alpha += Time.deltaTime / time;
            yield return null;
        }
    }
    public IEnumerator FadeIn(float time) {
        //realizamos esta accion durante una cantidad de tiempo limitada
        while (canvasGroup.alpha > 0) {
            canvasGroup.alpha -= Time.deltaTime / time;
            yield return null;
        }
    }
    #endregion
}
