﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.Combat;
public class IntroPortalActivation : MonoBehaviour
{
    #region ############## VARIABLES
    public GameObject introPortal;
  #endregion

  #region ############## EVENTS

    void Start()
    {
        introPortal = GameObject.Find("PortalIntro2");
        if (gameObject.GetComponent<Fighter>().isSword==false) {
            introPortal.GetComponent<BoxCollider>().enabled = true;
        }
        if (gameObject.GetComponent<Fighter>().isSword == true) {
            introPortal.GetComponent<BoxCollider>().enabled = false;
        }
    }


    void Update()
    {
        
    }
  #endregion

  #region ############# METHODS

  #endregion
}
