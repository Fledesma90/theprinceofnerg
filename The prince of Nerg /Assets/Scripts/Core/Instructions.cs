﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instructions : MonoBehaviour
{
  #region ############## VARIABLES
  #endregion

  #region ############## EVENTS

  #endregion

  #region ############# METHODS
    /// <summary>
    /// Hello! In order for this scene to work properly, you need to leave Intro deactivated
    /// and NormalWorld activated. Furthermore, if you want to play it again from the beginning
    /// you need to erase the save data. In case you can't find it, I've kindly put a script
    /// that creates a menu called RPG Save System, it is placed between the Cinemachine and
    /// the Window tab. 
    /// </summary>
    void ThisMethodIsJustForExplanation() {

    }
  #endregion
}
