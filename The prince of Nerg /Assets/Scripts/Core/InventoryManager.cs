﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.Saving;
public class InventoryManager : MonoBehaviour,ISaveable
{
    #region ############## VARIABLES
    [Header("ItemsImages")]
    public GameObject hudMoney;
    public GameObject hudKey;
    public GameObject hudWine;
    public GameObject hudDew;
    public GameObject hudPotion;
    [Header("Allowances")]
    public bool canSeeKey;
    public bool canSeeMoney;
    public bool canBuyWine;
    public bool canBuyDew;
    [Header("CanSee")]
    public bool isMoney;
    public bool isKey;
    public bool isWine;
    public bool isDew;
    public bool isPotion;

  #endregion

  #region ############## EVENTS

    void Start() {
        hudMoney = GameObject.Find("HudMoney");
        hudKey= GameObject.Find("HudKey");
        hudWine = GameObject.Find("HudWine");
        hudDew= GameObject.Find("HudDew");
        hudPotion = GameObject.Find("HudPotion");
        //comprobamos si la booleana es verdadera al cargar el nivel
        //dado que al interactuar con el objeto de juego lo activaremos
        //para que así no tengamos que estar verificándolo en el update
        UiCheck();
    }

    void Update()
    {
        
    }
    #endregion

    #region ############# METHODS
    /// <summary>
    /// veríficamos si tenemos los objetos en el HUD
    /// </summary>
    private void UiCheck() {
        if (isMoney) {
            hudMoney.SetActive(true);
        } else {
            hudMoney.SetActive(false);
        }
        if (isKey) {
            hudKey.SetActive(true);
        } else {
            hudKey.SetActive(false);
        }
        if (isWine) {
            hudWine.SetActive(true);
        } else {
            hudWine.SetActive(false);
        }
        if (isDew) {
            hudDew.SetActive(true);
        } else {
            hudDew.SetActive(false);
        }
        if (isPotion) {
            hudPotion.SetActive(true);
        } else {
            hudPotion.SetActive(false);
        }
    }
  /// <summary>
  /// variables a guardar en el struct para guardar data entre escenas
  /// </summary>
    [System.Serializable]
    struct InventorySaveData {
        public bool canSeeKey;
        public bool canSeeMoney;
        public bool canBuyWine;
        public bool canBuyDew;
        public bool isMoney;
        public bool isKey;
        public bool isWine;
        public bool isDew;
        public bool isPotion;
    }
    public object CaptureState() {

        InventorySaveData data = new InventorySaveData();
        data.canSeeKey = canSeeKey;
        data.canSeeMoney = canSeeMoney;
        data.canBuyWine = canBuyWine;
        data.canBuyDew = canBuyDew;
        data.isMoney = isMoney;
        data.isKey = isKey;
        data.isWine = isWine;
        data.isDew = isDew;
        data.isPotion = isPotion;
        return data;
    }

    public void RestoreState(object state) {

        InventorySaveData data = (InventorySaveData)state;
        canSeeKey = data.canSeeKey;
        canSeeMoney = data.canSeeMoney;
        canBuyWine = data.canBuyWine;
        canBuyDew = data.canBuyDew;
        isMoney = data.isMoney;
        isKey = data.isKey;
        isWine = data.isWine;
        isDew = data.isDew;
        isPotion = data.isPotion;
    }
    #endregion
}
