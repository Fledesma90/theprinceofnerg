﻿namespace theprinceofnerg.Core {
    //todo lo que tenga esta interfaz tiene que tener este método
    public interface IAction {
        //necesitan este método, este método es publico como todo en la interfaz
        void Cancel();
    }
}
