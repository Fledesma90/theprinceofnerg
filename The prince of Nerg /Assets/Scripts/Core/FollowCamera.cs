﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace theprinceofnerg.Core {
    public class FollowCamera : MonoBehaviour {
        #region ############## VARIABLES
        //objeto que seguira la cámara 
        [SerializeField] Transform player;

        #endregion

        #region ############## EVENTS

        void Start() {

        }


        void LateUpdate() {
            transform.position = player.position;
        }
        #endregion

        #region ############# METHODS

        #endregion
    }

}
