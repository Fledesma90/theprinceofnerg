﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using theprinceofnerg.SceneManagement;
public class Intro : MonoBehaviour
{
    #region ############## VARIABLES
    public string nextLevel;
    public int secondsToWait;
    //tiempo que dura la animación de fadeout
    [SerializeField] float fadeOutTime = 1f;
    //tiempo que dura el fade in
    [SerializeField] float fadeInTime = 2f;
    //tiempo de espera entre fade in y fade out
    [SerializeField] float fadeWaitTime = 0.5f;
    #endregion

    #region ############## EVENTS

    void Start()
    {
        
    }


    void Update()
    {
        
    }
    #endregion

    #region ############# METHODS
    private void OnTriggerEnter(Collider other) {
        if (other.tag=="FakePlayer") {
            StartCoroutine(TransitionIntro());
        }
    }

    private void LoadScene() {
        SceneManager.LoadScene(nextLevel);
    }

    private IEnumerator TransitionIntro() {
        //no destruimos el portal
        DontDestroyOnLoad(gameObject);
        //llamamos y buscamos al fader entre escenas
        Fader fader = FindObjectOfType<Fader>();
        yield return fader.FadeOut(fadeOutTime);
        //guardamos la escena actual
        SavingWrapper wrapper = FindObjectOfType<SavingWrapper>();
        wrapper.Save();
        yield return SceneManager.LoadSceneAsync(nextLevel);
        //cargamos la escena
        wrapper.Load();
        //una vez que se ha desplazado el personaje esperamos un tiempo para el fadein
        yield return new WaitForSeconds(fadeWaitTime);
        //realizamos el fadein porque ya hemos pasado a la siguiente escena
        yield return fader.FadeIn(fadeInTime);
        //destruimos este portal
        Destroy(gameObject);
    }

    #endregion
}
