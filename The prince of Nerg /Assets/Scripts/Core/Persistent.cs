﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace theprinceofnerg.Core {
    public class Persistent : MonoBehaviour {
        #region ############## VARIABLES
        //referencia al objeto fader
        [SerializeField] GameObject persistentObjectPrefab;
        //se ha instanciado el objeto
        static bool hasSpawned=false;
        #endregion

        #region ############## EVENTS
        private void Awake() {
            if (hasSpawned) return;
            SpawnPersistentObjects();
            hasSpawned = true;
        }
        void Start() {

        }


        void Update() {

        }
        #endregion

        #region ############# METHODS
        /// <summary>
        /// no destruimos el objeto entre escenas
        /// </summary>
        void SpawnPersistentObjects() {
            GameObject persistentObject = Instantiate(persistentObjectPrefab);
            DontDestroyOnLoad(persistentObject);
        }
        #endregion
    }
}


