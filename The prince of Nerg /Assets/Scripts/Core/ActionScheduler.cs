﻿
using UnityEngine;
//Gestiona la activación de las actions 
namespace theprinceofnerg.Core {
    public class ActionScheduler: MonoBehaviour {
        //action actualmente activa
        IAction currentAction;
        public void StartAction(IAction action) {
            //si la action actual es igual a la action no hace falta que realicemos nada más
            if (currentAction == action) return; 
            if (currentAction!=null) {
                currentAction.Cancel();

            }
            currentAction = action;
        }
    }
}

  
