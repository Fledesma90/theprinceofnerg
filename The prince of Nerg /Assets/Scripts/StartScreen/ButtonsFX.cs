﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsFX : MonoBehaviour
{
    #region ############## VARIABLES
    //referencia a los botones
    public GameObject startButton;
    public GameObject controlsButton;
    public GameObject optionsButton;
    public GameObject exitButton;
    //referencia al flash
    public GameObject white;
    //audiosource
    public AudioSource audioSource;
    //sonido especial
    public AudioClip whiteSound;
    //control de sonido
    public GameObject soundControl;
  #endregion

  #region ############## EVENTS

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        //activaremos le flash y los botones cuando se haya acabado la animación
        //de la presentación del juego
        startButton.SetActive(false);
        controlsButton.SetActive(false);
        optionsButton.SetActive(false);
        exitButton.SetActive(false);
        white.SetActive(false);
        Invoke("WhiteFlash", 14);
        Invoke("ActivateButtons", 14.5f);
    }


    void Update()
    {
        
    }
  #endregion

  #region ############# METHODS
    public void WhiteFlash() {
        white.SetActive(true);
        soundControl.SetActive(true);
        audioSource.PlayOneShot(whiteSound);
        Destroy(white, 1);
    }
    public void ActivateButtons() {
        startButton.SetActive(true);
        controlsButton.SetActive(true);
        optionsButton.SetActive(true);
        exitButton.SetActive(true);
    }
  #endregion
}
