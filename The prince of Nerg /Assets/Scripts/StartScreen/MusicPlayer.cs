﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MusicPlayer : MonoBehaviour
{
    #region ############## VARIABLES
    public AudioClip musicMenu;
    public AudioClip musicInGame;
    //referencia al audiosource
    private AudioSource audioSource;
    private static MusicPlayer musicPlayerInstance;
    #endregion

    #region ############## EVENTS
    private void Awake() {
        if (musicPlayerInstance == null) {
            DontDestroyOnLoad(this);
            musicPlayerInstance = this;
        } else {
            Destroy(gameObject);
        }

       

    }
    void Start() {
        audioSource = GetComponent<AudioSource>();
        if (SceneManager.GetActiveScene().name=="StartScreen") {
            audioSource.clip = musicMenu;
            audioSource.Play();
        }
    }

    #endregion

    #region ############# METHODS
    public void SetVolume(float volume) {
        audioSource.volume = volume;
    }
    public void ChangeMusic() {
        audioSource.clip = musicInGame;
        audioSource.Play();
    }
    #endregion
}
