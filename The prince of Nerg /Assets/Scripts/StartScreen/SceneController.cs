﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.SceneManagement;
//usamos la libreria scenemanagement para la gestión de escenas
using UnityEngine.SceneManagement;
using theprinceofnerg.Utils;
public class SceneController : MonoBehaviour
{
    #region ############## VARIABLES
    //referencia a la escena actual
    private int currentSceneIndex;
    
  #endregion

  #region ############## EVENTS

    void Start()
    {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    }


    void Update()
    {
        
    }
  #endregion

  #region ############# METHODS
    /// <summary>
    /// cambiamos de escena
    /// </summary>
    public void LoadNextScene() {

        //SceneManager.LoadScene(currentSceneIndex + 1);
        StartCoroutine(NextLevel());
    }
    public IEnumerator NextLevel() {
        DontDestroyOnLoad(gameObject);
        Fader fader = FindObjectOfType<Fader>();
        yield return fader.FadeOut(0.5f);
        SavingWrapper wrapper = FindObjectOfType<SavingWrapper>();
        wrapper.Save();
        yield return SceneManager.LoadSceneAsync(currentSceneIndex + 1);
        wrapper.Load();
        yield return new WaitForSeconds(1);
        yield return fader.FadeIn(0.5f);
        Destroy(gameObject);
    }
    public void ExitGame() {
        Application.Quit();
    }
  #endregion
}
