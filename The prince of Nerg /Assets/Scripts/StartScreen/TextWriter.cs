﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TextWriter : MonoBehaviour
{
    #region ############## VARIABLES
    //referencia al componente
    private Text uiText;
    //referencia al texto que vamos a escribir
    public string textToWrite;
    //indice de los caracteres
    private int characterIndex;
    //referencia al tiempo que tardan en aparecer los caracteres
    private float timePerCharacter;
    //contador
    private float timer;
    public AudioSource audioSource;
    public AudioClip audioClip;
    public ParticleSystem ps;
    public float x;
  #endregion

  #region ############## EVENTS

    void Start()
    {
        
    }


    void Update()
    {
        
        if (uiText!=null) {
            timer -= Time.deltaTime;
            if (timer<=0) {
                x += 0.01f;
                //mostramos el siguiente caracter
                timer += timePerCharacter;
                ps.transform.position += new Vector3(x, 0, 0);
                //incrementamos los caracteres mas uno
                characterIndex++;
                Muting();
                //asi una vez se ponga el caracter actual se pone el siguiente
                uiText.text = textToWrite.Substring(0, characterIndex);
                
                if (characterIndex>=textToWrite.Length) {
                    uiText = null;
                    return;
                }
            }
        }
    }
  #endregion

  #region ############# METHODS
    public void AddWriter(Text uiText, string textToWrite, float timePerCharacter) {
        this.uiText = uiText;
        this.textToWrite = textToWrite;
        this.timePerCharacter = timePerCharacter;

    }

    void Muting() {
        if (characterIndex != 4 && characterIndex != 11 && characterIndex != 14) {
            audioSource.PlayOneShot(audioClip);
            ps.Play();

        }
    }
  #endregion
}
