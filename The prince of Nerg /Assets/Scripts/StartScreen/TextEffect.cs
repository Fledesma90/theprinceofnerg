﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TextEffect : MonoBehaviour
{
    #region ############## VARIABLES
    //texto que contiene el título del juego
    private Text gameTitle;
    [SerializeField] private TextWriter textWriter;

    #endregion

    #region ############## EVENTS
    private void Awake() {
        gameTitle = transform.Find("gameTitle").GetComponent<Text>();
    }
    void Start()
    {
        Invoke("Textfx", 4f);
    }


    void Update()
    {
        
    }
  #endregion

  #region ############# METHODS
    void Textfx() {
        textWriter.AddWriter(gameTitle, "The Prince Of Nerg", 0.3f);

    }
    #endregion
}
