﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabController : MonoBehaviour
{
    #region ############## VARIABLES
    //pestaña que tenemos actualmente abierta
    public GameObject currentTab;
  #endregion

  #region ############## EVENTS

    void Start()
    {
        
    }


    void Update()
    {
        
    }
  #endregion

  #region ############# METHODS
    /// <summary>
    /// cambiamos de pestaña
    /// </summary>
    /// <param name="nextTab"></param>
    public void ChangeTab(GameObject nextTab) {
        currentTab.SetActive(false);
        currentTab = nextTab;
        currentTab.SetActive(true);
    }
  #endregion
}
