﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class CameraPriority : MonoBehaviour
{
    #region ############## VARIABLES
    public GameObject vcam2;

  #endregion

  #region ############## EVENTS

    void Start()
    {
        
    }


    void Update()
    {
        
    }
    #endregion

    #region ############# METHODS
    /// <summary>
    /// cambiamos la prioridad del cinemachine si está el player dentro o fuera
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            vcam2.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            vcam2.SetActive(false);
        }
    }
    #endregion
}
