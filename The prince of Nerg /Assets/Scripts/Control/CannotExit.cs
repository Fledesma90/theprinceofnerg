﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.Movement;
using theprinceofnerg.Control;
using theprinceofnerg.Combat;
public class CannotExit : MonoBehaviour
{
    #region ############## VARIABLES
    //dialogo del player al ver que no puede salir del cuarto
    [SerializeField] GameObject cannotExitCanvas;
    AudioSource audioSource;
    public AudioClip errorSound;
  #endregion

  #region ############## EVENTS

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        cannotExitCanvas.SetActive(false);
    }


    void Update()
    {
        
    }
    #endregion

    #region ############# METHODS
    private void OnTriggerEnter(Collider other) {
        if (other.tag=="Player") {
            //activamos corrutina con las órdenes pertinentes al player
            StartCoroutine(CannotExitCanvas());
            audioSource.PlayOneShot(errorSound);
        }
       
    }
    /// <summary>
    /// Si no puedo salir por no tener la espada se activará esta corrutina
    /// </summary>
    /// <returns></returns>
    IEnumerator CannotExitCanvas() {
        cannotExitCanvas.SetActive(true);
        GameObject player;
        player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<PlayerController>().canNotMove = true;
        player.GetComponent<Animator>().SetBool("Talk", true);
        yield return new WaitForSeconds(4.5f);
        player.GetComponent<PlayerController>().canNotMove = false;
        player.GetComponent<Animator>().SetBool("Talk", false);
        cannotExitCanvas.SetActive(false);
    }
    #endregion
}
