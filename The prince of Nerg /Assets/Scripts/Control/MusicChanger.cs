﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MusicChanger : MonoBehaviour
{
    #region ############## VARIABLES
    //musica del juego
    public GameObject musicPlayer;
    //nombre de la escena
    public string startMenu = "StartScreen";
    //canciones
    public AudioClip clip1;
    public AudioClip clip2;
  #endregion

  #region ############## EVENTS
    //verificamos en el start en que escena estamos y dependiendo de la escena
    //reproducimos una música u otra
    void Start()
    {
        musicPlayer = GameObject.Find("MusicPlayer");
        if (SceneManager.GetActiveScene().name=="StartScreen") {
            musicPlayer.GetComponent<AudioSource>().clip = clip1;
            musicPlayer.GetComponent<AudioSource>().Play();
        } else {
            musicPlayer.GetComponent<AudioSource>().clip = clip2;
            musicPlayer.GetComponent<AudioSource>().Play();
        }
    }


    void Update()
    {
       
    }
  #endregion

  #region ############# METHODS
    
  #endregion
}
