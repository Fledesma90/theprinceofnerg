﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    #region ############## VARIABLES
    //slider del audio de master
    public Slider masterSlider;
    //slider del audio de la música
    public Slider musicSlider;
    //slider del audio de los efectos de sonidos
    public Slider sfxSlider;
    //referencia al audiomixer
    public AudioMixer masterMixer;
    //singleton para que los valores persistan en las demás escenas
    public static SoundManager instance;
    #endregion

    #region ############## EVENTS
    /// <summary>
    /// si hay otro elemento igual lo destruimos
    /// </summary>
    void Awake()
    {
        if (instance == null) {
            DontDestroyOnLoad(this);
            instance = this;
        } else {
            Destroy(gameObject);
        }
        
    }


    void Update()
    {

    }
    #endregion

    #region ############# METHODS
    /// <summary>
    /// modificamos los valores del slider de Master
    /// </summary>
    public void SetMasterVolume()
    {
        masterMixer.SetFloat("Master", masterSlider.value);
    }

    /// <summary>
    /// modificamos los valores del slider de efectos de sonido
    /// </summary>
    public void SetEffectsVolume()
    {
        masterMixer.SetFloat("SFX", sfxSlider.value);
    }

    /// <summary>
    /// modificamos los valores del slider de la música
    /// </summary>
    public void SetMusicVolume()
    {
        masterMixer.SetFloat("Music", musicSlider.value);
    }
    #endregion
}
