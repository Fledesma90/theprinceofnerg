﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.UI;
using theprinceofnerg.Control;
using theprinceofnerg.Saving;
using theprinceofnerg.SceneManagement;
public class ShowHideUI : MonoBehaviour,ISaveable
{
    #region ############## VARIABLES
    //panel de pausa
    public GameObject UIContainer;
    //referencia al savingWrapper que se instanciará en cada escena para guardar los datos
    SavingWrapper savingWrapper;
    //bool que detendrá el movimiento del player
    public bool controllerOn;
    #endregion

    #region ############## EVENTS
  
    void Start()
    {
        //activamos los controles siempre para movernos
        controllerOn = true;
        //referencias de los objetos necesarios 
        savingWrapper = GameObject.Find("Saving").GetComponent<SavingWrapper>();
        UIContainer = GameObject.Find("PauseMenuUI");
        //desactivamos el menu de pausa al empezar
        UIContainer.SetActive(false);
    }


    void Update()
    {
        //nos podremos mover según el estado de la booleana
        if (controllerOn == true) {
            gameObject.GetComponent<PlayerController>().enabled = true;
        }
        if (controllerOn == false) {
            gameObject.GetComponent<PlayerController>().enabled = false;
        }
        Controls();
    }
    #endregion

    #region ############# METHODS
    public void Controls() {
        
        if (Input.GetButtonDown("Cancel")) {
            Pause();
        }
    }

    public void Pause() {
        UIContainer.SetActive(!UIContainer.activeSelf);
        controllerOn = !controllerOn;
    }
    //guardamos el estado del controller para que se guarde entre escenas
    public object CaptureState() {
        return controllerOn;
    }

    public void RestoreState(object state) {
        controllerOn = (bool)state;
    }
    /// <summary>
    /// Cerramos el juego
    /// </summary>
    public void QuitGame() {

        savingWrapper.Delete();
        Application.Quit();
    }

    #endregion
}
