﻿using System.Collections;
using System.Collections.Generic;
using theprinceofnerg.Combat;
using theprinceofnerg.Movement;
using theprinceofnerg.Conversation;
using theprinceofnerg.Prop;
using UnityEngine;
using UnityEngine.AI;
using theprinceofnerg.UI;
//utilizamos el namespace theprinceofnerg para las dependencias
namespace theprinceofnerg.Control {
    public class PlayerController : MonoBehaviour {
        #region ############## VARIABLES

        public Camera cam;
        public bool canNotMove;
        public GameObject pauseMenu;
        #endregion

        #region ############## EVENTS
       
        void Start() {
            cam = FindObjectOfType<Camera>();
            
        }


        void Update() {
            
            if (InteractWithCombat()) return;
            if (InteractWithCharacterThire()) return;
            if (InteractWithCharacterRika()) return;
            if (InteractWithCharacterHiedra()) return;
            if (InteractWithKingAndQueen()) return;
            if (InteractWithMovement()) return;
        }
  
        private bool InteractWithCombat() {
            RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
            //hacemos un foreach en los elementos de hits con la variable raycasthit
            foreach (RaycastHit hit in hits) {
                CombatTarget target = hit.transform.GetComponent<CombatTarget>();
                //seguimos con el loop si no se cumple la condición
                if (target == null) continue;
                    
     
                if (Input.GetMouseButtonDown(0)) {
                    GetComponent<Fighter>().Attack(target);                }
                //ponemos true aquí porque queremos que cambie el cursor
                //cuando podamos combatir
                return true;
            }
            //no hemos encontrado ningun objetivo de lucha
            return false;
        }
        #endregion

        #region ############# METHODS
        /// <summary>
        /// nos movemos hacia donde apunta el cursor con un rayo
        /// </summary>
        private bool InteractWithMovement() 
        {
            
                //información con la que chocamos con el raycast
                RaycastHit hit;
                //almacenamos la posicion donde choca el raycast
                //cuando has chocado nos movemos hacia la posición
                bool hasHit = Physics.Raycast(GetMouseRay(), out hit);
                if (hasHit == true && canNotMove==false) {
                    if (Input.GetMouseButton(0)) {
                         GetComponent<Movement.Mover>().StartMoveAction(hit.point);
                    }
                    return true;
                }
                //si no interactuamos con nada hacia donde podamos movernos no se moverá el player
            return false;
        }
        /// <summary>
        /// lanzamos el rayo y si golpea al componente llamaremos al método pertinente de
        /// PlayerConversation
        /// </summary>
        /// <returns></returns>
        private bool InteractWithCharacterThire() {
            RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
            //hacemos un foreach en los elementos de hits con la variable raycasthit
            foreach (RaycastHit hit in hits) {
                DialogueManager target = hit.transform.GetComponent<DialogueManager>();

                //seguimos con el loop si no se cumple la condición
                if (target == null) continue;

                if (Input.GetMouseButtonDown(0)) {
                    GetComponent<PlayerConversation>().ThireConversation(target);
                    //target.GetComponent<DialogueManager>().dialoguesThireNumber -= 1;
                }

                return true;
            }
            return false;
        }

        private bool InteractWithCharacterRika() {
            RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
            //hacemos un foreach en los elementos de hits con la variable raycasthit
            foreach (RaycastHit hit in hits) {
                RikaDialogue target = hit.transform.GetComponent<RikaDialogue>();
                //seguimos con el loop si no se cumple la condición
                if (target == null) continue;
                if (Input.GetMouseButtonDown(0)) {
                    GetComponent<PlayerConversation>().RikaConversation(target);
                }

                return true;
            }
            return false;
        }

        private bool InteractWithCharacterHiedra() {
            RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
            //hacemos un foreach en los elementos de hits con la variable raycasthit
            foreach (RaycastHit hit in hits) {
                HiedraDialogue target = hit.transform.GetComponent<HiedraDialogue>();
                //seguimos con el loop si no se cumple la condición
                if (target == null) continue;
                if (Input.GetMouseButtonDown(0)) {
                    GetComponent<PlayerConversation>().HiedraConversation(target);
                }

                return true;
            }
            return false;
        }

        private bool InteractWithKingAndQueen() {
            RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
            foreach (RaycastHit hit in hits) {
                MercurioDialogue target = hit.transform.GetComponent<MercurioDialogue>();
                if (target == null) continue;
                if (Input.GetMouseButtonDown(0)) {
                    GetComponent<PlayerConversation>().KingQueenConver(target);
                }
                return true;
            }
            return false;
        }
        /// <summary>
        /// lanzamos un rayo desde la cámara al hacer click
        /// </summary>
        /// <returns></returns>
        private Ray GetMouseRay() {
            return cam.ScreenPointToRay(Input.mousePosition);
        }


        #endregion
    }

}