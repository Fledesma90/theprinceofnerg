﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using theprinceofnerg.Control;
namespace theprinceofnerg.UI {
    public class PauseMenuUI : MonoBehaviour {
        #region ############## VARIABLES
        
        #endregion

        #region ############## EVENTS

        void Start() {
            
        }


        void Update() {

        }

        private void OnEnable() {
            Time.timeScale = 0;
            
        }

        private void OnDisable() {
            Time.timeScale = 1;
            
        }
        #endregion

        #region ############# METHODS

        #endregion
    }
}

