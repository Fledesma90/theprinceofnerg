﻿namespace theprinceofnerg.Utils
{
    /// <summary>
    /// Clase contenedora que envuelve un valor y asegura que si inicializacion
    /// es llamada justo antes de su primer uso.
    /// </summary>
    public class LazyValue<T>
    {
        private T _value;
        private bool _initialized = false;
        private InitializerDelegate _initializer;

        public delegate T InitializerDelegate();

        /// <summary>
        /// establece el contenedor pero no inicializa el valor todavia.
        /// </summary>
        /// <param name="initializer"> 
        /// El delegado inicializador a llamar cuando se usa por primera vez.
        /// </param>
        public LazyValue(InitializerDelegate initializer)
        {
            _initializer = initializer;
        }

        /// <summary>
        /// El Get o set el contenido de este contenedor.
        /// </summary>
        /// <remarks>
        /// El valor del set antes de la inicializacion inicializará la clase.
        /// </remarks>
        public T value
        {
            get
            {
                // Ensure we init before returning a value.
                ForceInit();
                return _value;
            }
            set
            {
                // Don't use default init anymore.
                _initialized = true;
                _value = value;
            }
        }

        /// <summary>
        /// Fuerza la inicializacion del valor con un delegate.
        /// </summary>
        public void ForceInit()
        {
            if (!_initialized)
            {
                _value = _initializer();
                _initialized = true;
            }
        }
    }
}